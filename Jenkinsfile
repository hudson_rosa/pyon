pipeline {
    agent any
    options {
        skipStagesAfterUnstable()
    }
    environment {
        projectName = 'Pyon - Python Behave Test Automation'
        emailTo = 'hudsonssrosa@gmail.com'
        emailBody = 'Check console output at $BUILD_URL to view the results. \n\n ${CHANGES} \n\n ${BUILD_LOG, maxLines=100, escapeHtml=false}'
    }
    stages {
        stage('Setting Environment for Testing') {
            steps {
                bat 'python --version'
                bat 'python -m pip install pipenv'
                bat 'python -m venv venv'
                bat 'python -m pip install --upgrade pip --user'
                bat 'python -m pip install behave --user'
                bat 'python -m pip install -r requirements.txt --user'
            }
        }
        stage('Running Python-Behave') {
            steps {
                bat 'python behave_runner.py'
            }
        }
    }
    post {
        success {
            emailext body: "${emailBody}",
                    to: "${emailTo}",
                    subject: 'Build SUCCESS in Jenkins: $PROJECT_NAME - #$BUILD_NUMBER'
        }
        failure {
            emailext body: "${emailBody}",
                    to: "${emailTo}",
                    subject: 'Build FAILED in Jenkins: $PROJECT_NAME - #$BUILD_NUMBER'
        }
        unstable {
            emailext body: "${emailBody}",
                    to: "${emailTo}",
                    subject: 'UNSTABLE build in Jenkins: $PROJECT_NAME - #$BUILD_NUMBER'
        }
    }
}