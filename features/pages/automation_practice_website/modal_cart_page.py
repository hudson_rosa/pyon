from features.factory.web.web_impl import Web
from selenium.webdriver.common.by import By


class ModalCartPage(Web):
    loc_modal_cart_adding = (By.XPATH, '//*[@id="layer_cart"]/div[1]/div[2]')
    loc_button_proceed_to_checkout = (By.XPATH, '//*[@id="layer_cart"]/div[1]/div[2]/div[4]/a')

