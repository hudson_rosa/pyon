from features.factory.web.web_impl import Web
from selenium.webdriver.common.by import By


class CartSummaryPage(Web):
    loc_label_total_product = (By.ID, 'total_product')
    loc_label_total_shipping = (By.ID, 'total_shipping')
    loc_label_total_price = (By.ID, 'total_price')

    def get_label_total_product(self, value_expected):
        CartSummaryPage.get_label_or_text_by(self, value_expected, *CartSummaryPage.loc_label_total_product)

    def get_label_total_shipping(self, value_expected):
        CartSummaryPage.get_label_or_text_by(self, value_expected, *CartSummaryPage.loc_label_total_shipping)

    def get_label_total_price(self, value_expected):
        CartSummaryPage.get_label_or_text_by(self, value_expected, *CartSummaryPage.loc_label_total_price)
