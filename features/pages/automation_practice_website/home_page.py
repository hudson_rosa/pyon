from features.factory.web.web_impl import Web
from selenium.webdriver.common.by import By


class HomePage(Web):
    loc_text_search = (By.ID, 'search_query_top')
    loc_button_search = (By.XPATH, '//*[@id="searchbox"]/button')
    loc_label_product_name = (By.XPATH, '//*[@id="center_column"]/ul/li/div/div[2]/h5/a[contains(text(),"Blouse")]')
    loc_label_product_unit_price = (By.XPATH, '//*[@id="center_column"]/ul/li/div/div[2]/div[1]/span')
    loc_hover_product = (By.CSS_SELECTOR, '#center_column > ul > li')
    loc_button_add_to_cart = (By.CSS_SELECTOR, '#center_column > ul > li > div > div.right-block > div.button-container > a.button.ajax_add_to_cart_button.btn.btn-default > span')
