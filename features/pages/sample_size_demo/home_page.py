from features.factory.mobile.mob import Mob
from appium.webdriver.common.mobileby import MobileBy


class HomePage(Mob):
    loc_button_home = (MobileBy.ID, 'tab-t0-0')
    loc_text_population = (MobileBy.XPATH, '//*[@class="android.widget.TextView" and @text="10000"]')
    loc_button_calculate = (MobileBy.XPATH, '“//android.widget.Button[contains(text(),"SAMPLE SIZE NEEDED")]')

    def get_home_tab(self):
        HomePage.get_element_by(self, *HomePage.loc_button_home)
        HomePage.force_sleep(self, 2)

    def click_home_tab(self):
        HomePage.click_on_element_by(self, *HomePage.loc_button_home)

    def type_text_population(self, value_expected):
        HomePage.click_on_element_by(self, *HomePage.loc_text_population)
        HomePage.type_text_by(self, value_expected, *HomePage.loc_text_population)

    def get_calculation(self, value_expected):
        HomePage.click_on_element_by(self, (MobileBy.XPATH, f'“//android.widget.Button[contains(text(),"SAMPLE SIZE NEEDED : {value_expected}")]'))
