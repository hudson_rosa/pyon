from features.factory.mobile.mob import Mob
from appium.webdriver.common.mobileby import MobileBy


class FormulasPage(Mob):
    loc_button_formulas = '//android.view.View[@resource-id="tab-t0-2"]'

    def get_formulas_tab(self):
        FormulasPage.force_sleep(self, 5)
        FormulasPage.get_element_by_xpath(self, FormulasPage.loc_button_formulas)
        FormulasPage.force_sleep(self, 2)

    def click_formulas_tab(self):
        FormulasPage.click_on_element_by_xpath(self, *FormulasPage.loc_button_formulas)
