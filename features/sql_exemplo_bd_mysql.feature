# language: pt
@db_test
Funcionalidade: Conexão, preenchimento e seleção de registros de "funcionalidades" em um banco de bados MySQL

    Contexto: Limpando o database 'pyon_schema_tests'
        Dado que o usuário se conecta ao banco de dados
        E o usuário apaga e recria a base de dados com as instruções SQL

    Cenário: O usuário cria tabelas, preenche-as e consulta registros
        Dado que o usuário cria as tabelas tb_test_resource, tb_test_type, tb_category e tb_feature
        Quando o usuário preenche estas tabelas
        Então o usuário pode consultar com "join" entre as tabelas tb_feature, tb_test_type e tb_category
        E o resultado deverá apresentar "4" registros de funcionalidades
