import requests
import urllib3
from utils.TextColorUtil import TextColor as Color
from features.factory.validation.logging.base_logging import BaseLogging
from settings.environment_general_settings import Settings as Conf
from urllib3.connection import HTTPConnection

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


class HttpSetup:

    @staticmethod
    def setup_http_version(auth_key, token, url):
        request_timeout = int(Conf.get_api_request_timeout())
        if auth_key is not None:
            try:
                HTTPConnection._http_vsn = 11
                HTTPConnection._http_vsn_str = 'HTTP/1.1'
                return requests.get(url,
                                    headers={auth_key: token},
                                    verify=False,
                                    params="",
                                    timeout=request_timeout)
            except requests.exceptions.ChunkedEncodingError:
                print("------->  Http Version/1.0")
                HTTPConnection._http_vsn = 10
                HTTPConnection._http_vsn_str = 'HTTP/1.0'
                return requests.get(url,
                                    headers={auth_key: token},
                                    verify=False,
                                    params="",
                                    timeout=request_timeout)
            except Exception as exc:
                BaseLogging.log_status_exception(exc, Color.red("--- FAILED --- Caused by: "))
        else:
            try:
                HTTPConnection._http_vsn = 11
                HTTPConnection._http_vsn_str = 'HTTP/1.1'
                return requests.get(url, verify=False, params="", timeout=request_timeout)
            except requests.exceptions.ChunkedEncodingError:
                print("------->  Http Version/1.0")
                HTTPConnection._http_vsn = 10
                HTTPConnection._http_vsn_str = 'HTTP/1.0'
                return requests.get(url, verify=False, params="", timeout=request_timeout)
            except Exception as exc:
                BaseLogging.log_status_exception(exc, Color.red("--- FAILED --- Caused by: "))
        return ""
