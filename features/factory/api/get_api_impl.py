import json
import jsonschema
import time
import urllib3
from jsonschema import validate
from features.factory.validation.logging.base_logging import BaseLogging
from features.factory.api.http_setup import HttpSetup
from utils.TextColorUtil import TextColor as Color
from utils.StringsUtil import StringUtil as StrUtil

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


class GETStep(BaseLogging, HttpSetup):

    def __init__(self):
        super(GETStep, self).__init__()

    @staticmethod
    def get_response(url, auth_key, token):
        response = GETStep.setup_http_version(auth_key, token, url)
        return [json.loads(response.text)]

    """
    If you want to check a GET Http request from an API, you will need to fill some parameters in the specific validation method, as shown bellow:
                      url:  If you set the IP and PORT in the 'env_settings' file, you just need to set the rest of API url STRING here.
               iterations:  If you want to repeat as many times as you want, so you need to change this INT value.
                 auth_key:  Set the correct authentication key in the 'env_settings' file and ensure that parameter is set in main() and test() methods.
                    token:  Set the correct token in the 'env_settings' file and ensure that parameter is set in main() and test() methods.
                extra_key:  To improve your validation, please set the STRING parameter 'extra_key' to check that the same key is present in the response.
               parent_key:  If you need to check key returns in a hierarchical structure of a json response you will need to set a STRING as the parent key of the tree.
                child_key:  If you need to check a present child key in a hierarchical structure with 2 levels of a json, fill the STRINGS 'parent_key' and 'child_key'
                            parameter. But, if you need to validate an hierarchy with more than 2 levels, use only the 'child_key' and set the keys like this:
                            "< parent_key_name >.< second_level_key >.< third_level_key >.< last_and_child_key >"
     first_value_expected:  This STRING parameter is responsible to check if the value of the 'parent_key'/'child_key' in the response corresponds to the expected.
                            If you need to validate short char/number values expected with precision, you need to insert "%%" before or after the string.
          time_wait_start:  Use this STRING parameter to set a time in seconds to wait before the test execution.
            time_wait_end:  Use this STRING parameter to set a time in seconds to wait after the test execution.
                   schema:  If you need to validate the structure of a json response, you can set it in a string variable.
            response_body:  To validate the json schema, you will need to set the response body to a string variable, before the schema. 
    """

    """
        *** Use this method to validate requests if want to know if the response has 200 status. ***
    """
    @staticmethod
    def assert_health_check_is_200(url, iterations, auth_key, token, extra_key):
        for i in range(0, iterations):
            response = GETStep.setup_http_version(auth_key, token, url)
            GETStep.iteration_number(i)
            GETStep.log_header_body(response)
            GETStep.log_response_body(response)
            response_status = response.status_code
            json_resp = json.loads(response.text)
            success_req = "Successful request!"
            success_req_but_no_reg = "Successful request, but there is no data to be shown!"
            try:
                if 'rows' in json_resp:
                    if response_status == 200 and int(json_resp['rows']) == 0:
                        assert response_status == 200
                        GETStep.log_status_failed(response, None, success_req_but_no_reg)
                    elif response_status == 200 and int(json_resp['rows']) > 0:
                        GETStep.get_assert_for_200(response, response_status, success_req)
                    elif response_status == 200 and extra_key != "":
                        if extra_key in json_resp:
                            GETStep.get_assert_for_200(response, response_status, success_req)
                    elif response_status == 200 and extra_key == "":
                        GETStep.get_assert_for_200(response, response_status, success_req)
                    elif response_status == 202:
                        GETStep.get_assert_for_202(response, response_status, success_req)
                else:
                    if response_status == 200 and extra_key != "":
                        if extra_key in json_resp:
                            GETStep.get_assert_for_200(response, response_status, success_req)
                    elif response_status == 200 and extra_key == "":
                        GETStep.get_assert_for_200(response, response_status, success_req)
                    elif response_status == 202:
                        GETStep.get_assert_for_202(response, response_status, success_req)
            except KeyError:
                key = json_resp[extra_key]
                if response_status == 200 and key:
                    GETStep.get_assert_for_200(response, response_status, success_req)
            except Exception as exc:
                GETStep.log_status_failed(response, exc, "")

    @staticmethod
    def get_assert_for_200(response, response_status, success_req):
        assert response_status == 200
        GETStep.log_status_passed(response, success_req)

    @staticmethod
    def get_assert_for_202(response, response_status, success_req):
        assert response_status == 202
        GETStep.log_status_passed(response, success_req)

    """
        *** Use this method to validate the 200 status in a response an specific value expected from a key parameter ('parent_key' and/or 'child_key'). ***
    """
    @staticmethod
    def assert_key_contains_value(url, iterations, auth_key, token, parent_key, child_key, first_value_expected):
        for i in range(0, iterations):
            response = GETStep.setup_http_version(auth_key, token, url)
            GETStep.iteration_number(i)
            GETStep.log_header_body(response)
            GETStep.log_response_body(response)
            json_resp = json.loads(response.text)
            message_parent_success = f"The parameter value '{parent_key}' matches the expected value: '{first_value_expected}'"
            message_child_success = f"The parameter value '{child_key}' matches the expected value: '{first_value_expected}'"
            first_value_expected = str(first_value_expected)
            flag_partial_str_expected = False
            if str(first_value_expected).lower().strip().__contains__("%%"):
                first_value_expected = first_value_expected.replace("%%", "")
                flag_partial_str_expected = True
            try:
                if parent_key != "":
                    json_resp_parent_key = json_resp[parent_key]
                    if parent_key != "" and child_key == "" and 'rows' in json_resp:
                        message_failed = f"The parameter value found ({str(json_resp_parent_key)}) from key '{str(parent_key)}' it's not equals or " \
                            f"does not contains the expected value: '{str(first_value_expected)}'"
                        first_item_value_expected = str(first_value_expected).lower().strip()
                        GETStep.check_if_parent_key_contains_a_value_expected(
                            flag_partial_str_expected, first_item_value_expected, json_resp_parent_key, message_failed, message_parent_success, response)
                    elif parent_key != "" and child_key == "" and 'rows' not in json_resp:
                        message_failed = f"The parameter value found ({str(json_resp_parent_key)}) from key '{str(parent_key)}' it's not equals or " \
                            f"does not contains the expected value: '{str(first_value_expected)}'"
                        first_item_value_expected = str(first_value_expected).lower().strip()
                        GETStep.check_if_parent_key_contains_a_value_expected(
                            flag_partial_str_expected, first_item_value_expected, json_resp_parent_key, message_failed, message_parent_success, response)
                    elif parent_key != "" and 'rows' in json_resp and json_resp['rows'] > 0:
                        first_item = json_resp_parent_key[0]
                        message_failed = f"The parameter value found ({str(first_item[child_key])}) from key '{str(child_key)}' it's not equals or " \
                            f"does not contains the expected value: '{str(first_value_expected)}'"
                        GETStep.check_if_child_key_contains_a_value_expected(
                            flag_partial_str_expected, child_key, first_item, first_value_expected, message_child_success, message_failed, response)
                    elif parent_key != "" and 'rows' not in json_resp:
                        first_item = json_resp_parent_key[0]
                        message_failed = f"The parameter value found ({str(first_item[child_key])}) from key '{str(child_key)}' it's not equals or " \
                            f"does not contains the expected value: '{str(first_value_expected)}'"
                        GETStep.check_if_child_key_contains_a_value_expected(
                            flag_partial_str_expected, child_key, first_item, first_value_expected, message_child_success, message_failed, response)
                else:
                    items = json.loads(response.text)
                    list_child: object = child_key.split(".")
                    first_item = items[list_child[0]]
                    next = i + 1
                    if list_child[next] is not None:
                        concatenated_item = first_item[0][list_child[next]]
                        nested_item = ""
                        while next <= len(list_child) - 1:
                            next = next + 1
                            nested_item = concatenated_item[0]
                        message_failed = f"The parameter value found ({str(nested_item)}) from key '{child_key}' it's not equals or " \
                            f"does not contains the expected value: '{first_value_expected}'"
                        nested_item_tree = str(nested_item).lower().strip()
                        first_item_value_expected = str(first_value_expected).lower().strip()
                        GETStep.check_if_child_in_the_tree_contains_a_value_expected(first_item_value_expected, message_child_success, nested_item_tree, response)
                        if str(nested_item_tree).__contains__(first_item_value_expected):
                            assert str(nested_item_tree).__contains__(first_item_value_expected)
                            GETStep.log_status_passed(response, message_child_success)
                        else:
                            GETStep.log_status_failed(response, None, message_failed)
            except KeyError:
                GETStep.log_status_failed(response, None, f"Request not processed with expected validation information because the keys "
                                                              f"'{parent_key}' and/or '{child_key}' were not found!")
            except Exception as exc:
                json_resp_child_key = json_resp[child_key]
                message_child_success = f"The exception is being handled. Found: '{json_resp_child_key}'. Expected (contains): '{first_value_expected}'"
                if str(json_resp_child_key).lower().strip().__contains__(str(first_value_expected).lower().strip()):
                    assert str(json_resp_child_key).lower().strip().__contains__(str(first_value_expected).lower().strip())
                    GETStep.log_status_passed(response, message_child_success)
                else:
                    GETStep.log_status_failed(response, exc, "No data were found to evaluate the requisition items!")

    @staticmethod
    def check_if_parent_key_contains_a_value_expected(flag, first_item_value_expected, json_resp_parent_or_child_key, message_failed, message_success, response):
        if flag:
            """Only In case of simple values expected as one char and that is a precisely value, for example"""
            if str(json_resp_parent_or_child_key).lower().strip() == str(first_item_value_expected).lower().strip():
                assert str(json_resp_parent_or_child_key).lower().strip() == str(first_item_value_expected).lower().strip()
                GETStep.log_status_passed(response, message_success)
            else:
                GETStep.log_status_failed(response, None, message_failed)
        else:
            """In any case of string validation, except for one char in the value expected parameter"""
            if str(json_resp_parent_or_child_key).lower().strip() == str(first_item_value_expected).lower().strip() \
                    or str(json_resp_parent_or_child_key).lower().strip().__contains__(str(first_item_value_expected).lower().strip()):
                assert str(json_resp_parent_or_child_key).lower().strip().__contains__(str(first_item_value_expected).lower().strip())
                GETStep.log_status_passed(response, message_success)
            else:
                GETStep.log_status_failed(response, None, message_failed)

    @staticmethod
    def check_if_child_key_contains_a_value_expected(flag, child_key, first_item, first_value_expected, message_success, message_failed, response):
        if child_key in first_item and child_key != "":
            first_child_item = first_item[child_key]
            GETStep.check_if_parent_key_contains_a_value_expected(flag, first_value_expected, first_child_item, message_failed, message_success, response)

    """
        *** Use this method to validate if response doesn't returns any results, but the status should be 200. ***
    """
    @staticmethod
    def assert_health_check_is_empty_but_200(url, iterations, auth_key, token):
        for i in range(0, iterations):
            response = GETStep.setup_http_version(auth_key, token, url)
            GETStep.iteration_number(i)
            GETStep.log_header_body(response)
            GETStep.log_response_body(response)
            response_status = response.status_code
            json_resp = json.loads(response.text)
            failed_req = "Request didn't return the expected status!"
            success_req_but_no_reg = "Successful request. No data found!"
            try:
                rows = json_resp['rows']
                if response_status == 200 and 'rows' in json_resp and int(rows) == 0:
                    GETStep.get_assert_for_200(response, response_status, success_req_but_no_reg)
                elif response_status != 400:
                    assert response_status == 400
                    GETStep.log_status_failed(response, None, failed_req)
            except KeyError:
                if response_status == 200:
                    GETStep.get_assert_for_200(response, response_status, success_req_but_no_reg)
            except Exception as exc:
                GETStep.log_status_failed(response, exc, response_status)

    """
        *** Use this method to validate ordering between determined 'parent_key' and/or 'child_key'. The status should be 200. ***
    """
    @staticmethod
    def assert_value_ordering_in_response(url, iterations, auth_key, token, parent_key, child_key):
        for i in range(0, iterations):
            response = GETStep.setup_http_version(auth_key, token, url)
            GETStep.iteration_number(i)
            GETStep.log_header_body(response)
            GETStep.log_response_body(response)
            json_resp = json.loads(response.text)
            try:
                json_resp_parent_key = json_resp[parent_key]
                first_item = json_resp_parent_key[0]
                second_item = json_resp_parent_key[1]
                last_item = json_resp_parent_key[len(json_resp_parent_key)-1]
                message_for_equals = f"The parameter values '{child_key}' are equals between others registers and/or not everyone has that same key"
                message_for_desc = f"The parameter values are sorted in DESCENDING by the '{child_key}' key"
                message_for_asc = f"The parameter values are sorted in ASCENDING by the '{child_key}' key"
                has_desc_param_ = str(url).__contains__('=Desc') or str(url).__contains__('=desc')
                has_asc_param_ = str(url).__contains__('=Asc') or str(url).__contains__('=asc')
                first_child_item = str(first_item[child_key]).lower().strip()
                second_child_item = str(second_item[child_key]).lower().strip()
                last_child_item = str(last_item[child_key]).lower().strip()
                if child_key in last_item and child_key != "":
                    GETStep.check_if_child_in_the_tree_contains_a_value_expected(last_child_item, message_for_equals, first_child_item, response)
                    if has_desc_param_:
                        if first_child_item > last_child_item:
                            assert first_child_item > last_child_item
                            GETStep.log_status_passed(response, message_for_desc)
                    if has_asc_param_:
                        if first_child_item < last_child_item:
                            assert first_child_item < last_child_item
                            GETStep.log_status_passed(response, message_for_asc)
                else:
                    GETStep.check_if_child_in_the_tree_contains_a_value_expected(second_child_item, message_for_equals, first_child_item, response)
                    if has_desc_param_:
                        if first_child_item > second_child_item:
                            assert first_child_item > second_child_item
                            GETStep.log_status_passed(response, message_for_desc)
                    if has_asc_param_:
                        if first_child_item < second_child_item:
                            assert first_child_item < second_child_item
                            GETStep.log_status_passed(response, message_for_asc)
            except KeyError:
                GETStep.log_status_failed(response, None, f"Request not processed with expected validation information because the keys "
                                                          f"'{parent_key}' and/or '{child_key}' were not found!")
            except Exception as exc:
                GETStep.log_status_failed(response, exc, "It's not possible to evaluate the sorting in the request!")

    @staticmethod
    def check_if_child_in_the_tree_contains_a_value_expected(item_value_expected, message_child_success, nested_item_tree, response):
        if nested_item_tree == item_value_expected:
            assert nested_item_tree == item_value_expected
            GETStep.log_status_passed(response, message_child_success)

    """
        *** Use this method to validate an invalid parameter that was set in the URL and response should be the 400 status. ***
    """
    @staticmethod
    def assert_invalid_property_option_is_400(url, iterations, auth_key, token):
        for i in range(0, iterations):
            response = GETStep.setup_http_version(auth_key, token, url)
            GETStep.iteration_number(i)
            GETStep.log_header_body(response)
            GETStep.log_response_body(response)
            response_status = response.status_code
            json_resp = json.loads(response.text)
            try:
                if response_status == 400:
                    response_error = json_resp['errors']
                    assert response_status == 400
                    assert response_error, "Invalid option on key entered in URL!"
                    GETStep.log_status_passed(response, "Bad Request! Success result.")
                elif response_status == 200 and 'rows' in json_resp and int(json_resp['rows']) == 0:
                    assert response_status == 200
                    GETStep.log_status_failed(response, None, "No valid data were found!")
                elif response_status != 400:
                    assert response_status != 400
                    GETStep.log_status_failed(response, None, "Request may have been successful, but does not returned the expected result!")
            except Exception as exc:
                GETStep.log_status_failed(response, exc, response_status)

    """
        *** Use this method to validate an unauthorized authentication and response should be the 401 status. ***
    """
    @staticmethod
    def assert_unauthorized_access_is_401(url, iterations, auth_key, token):
        for i in range(1, iterations + 1):
            response = GETStep.setup_http_version(auth_key, token, url)
            GETStep.iteration_number(i)
            GETStep.log_header_body(response)
            GETStep.log_response_body(response)
            response_status = response.status_code
            try:
                if response_status == 401:
                    assert response_status == 401
                    GETStep.log_status_passed(response, "Not authorized request! Success result.")
                    break
                elif response_status == 200:
                    assert response_status == 200
                    GETStep.log_status_failed(response, None, "The requisition may have been authorized, but should be 401 status!")
            except Exception as exc:
                GETStep.log_status_failed(response, exc, response_status)

    """
        *** Use this method to validate an invalid URL and response should be the 404 status. ***
    """
    @staticmethod
    def assert_invalid_url_is_404(url, iterations, auth_key, token):
        for i in range(1, iterations + 1):
            response = GETStep.setup_http_version(auth_key, token, url)
            GETStep.iteration_number(i)
            GETStep.log_header_body(response)
            GETStep.log_response_body(response)
            response_status = response.status_code
            try:
                if response_status == 404:
                    assert response_status == 404
                    GETStep.log_status_passed(response, "Request not found by server! Success result.")
                    break
                elif response_status == 200:
                    assert response_status == 200
                    GETStep.log_status_failed(response, None, "Request was authorized successfully!")
            except Exception as exc:
                GETStep.log_status_failed(response, exc, "")

    """
        *** Use this method to validate 'X-RateLimit-Limit' during many requests performed during an invalid URL and response should be the 404 status. ***
    """
    @staticmethod
    def assert_x_rate_limit_is_429(url, iterations, minutes, auth_key, token, time_wait_start, time_wait_end):
        iterations = int(iterations)
        minutes = float(minutes)
        GETStep.waiting_for(time_wait_start)
        start_time = time.time()
        seconds = minutes*60
        try:
            for i in range(1, iterations + 1):
                stopwatch = GETStep.stopwatch(minutes, start_time)
                response = GETStep.setup_http_version(auth_key, token, url)
                GETStep.iteration_number(i)
                response_status = response.status_code
                message_no_limits = f"Only {i} requests was sent, {minutes} minute(s) has passed and the 'X-RateLimit-Limit' was not extrapolated!"
                if response_status == 429:
                    assert response_status == 429
                    GETStep.log_status_passed(response, f"The 'X-RateLimit-Limit' has reached the limit of requests per {minutes} minute(s)!")
                    break
                elif response_status == 200:
                    assert response_status == 200
                    GETStep.log_assert_message(response_status, Color.blue("The 'X-RateLimit-Limit' accepts requests!"))
                    if stopwatch > seconds:
                        GETStep.log_status_failed(response, None, message_no_limits)
                        break
        except Exception as exc:
            GETStep.log_assert_message(None, Color.red(f"The number of requests was not satisfied in {minutes} minute(s)!"))
            GETStep.log_status_exception(exc, None)
        GETStep.waiting_for(time_wait_end)

    @staticmethod
    def stopwatch(minutes, start):
        min_to_sec = int(minutes * 60)
        elapsed = 0
        while elapsed < min_to_sec:
            elapsed = time.time() - start
            print("------->  %02d seconds elapsed" % elapsed)
            time.sleep(1)
            return elapsed

    """
        *** Use this method to validate API Contract tests just passing the JSON_SCHEMA and RESPONSE_BODY. ***
    """
    @staticmethod
    def assert_json_schema(schema, response_body):
        for idx, data in enumerate(response_body):
            try:
                validate(data, schema)
                GETStep.log_assert_message(None, Color.green("The json structure matches the response body!"))
            except (jsonschema.exceptions.ValidationError, jsonschema.exceptions.FormatError, Exception) as err:
                GETStep.log_assert_message(None, Color.red("Something is wrong between the contract and response!"))
                print(Color.red(StrUtil.indent_dict_body(schema)))
                GETStep.log_status_exception(err, StrUtil.indent_dict_body(schema))
