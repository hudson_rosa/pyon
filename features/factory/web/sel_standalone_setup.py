import os
import multiprocessing
import subprocess
import time
from settings.environment_general_settings import Settings as Conf
from features.factory.validation.exception.RunningException import RunningException as RExc
from utils.TextColorUtil import TextColor as Color
from utils.OsUtil import OsUtil

SEP = os.sep


class GridFactory:

    @staticmethod
    def start_new_thread_for_selenium_server():
        thread = multiprocessing.Process(target=GridFactory.run_selenium_standalone())
        return thread

    @staticmethod
    def run_selenium_standalone():
        try:
            sel_stand_resources_dir = Conf.get_resources_path_selenium_standalone()
            sel_grid_register_url = Conf.get_web_remote_url()
            print(Color.yellow("Selenium Standalone server is starting..."))
            if not sel_stand_resources_dir.__contains__(".jar"):
                sel_stand_resources_dir = sel_stand_resources_dir + ".jar"
            subprocess.Popen(f"java -jar {sel_stand_resources_dir} -role hub")
            subprocess.Popen(f"java -jar {sel_stand_resources_dir} -role node -hub {sel_grid_register_url}/grid/register/ -port 3456")
            time.sleep(3)
        except Exception as ex:
            GridFactory.force_kill_java_process()
            RExc.with_raising_error("It's not possible to execute the Selenium Server Standalone!", ex)

    @staticmethod
    def force_kill_java_process():
        print(Color.yellow("\nKilling Java process..."))
        if OsUtil.has_os_platform_name("win"):
            OsUtil.send_command_to_os("taskkill /F /IM java.exe /T")
        else:
            OsUtil.send_command_to_os("killall java")

    @staticmethod
    def kill_thread(current_thread):
        current_thread.terminate()
        current_thread.join()
