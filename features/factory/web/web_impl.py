import os.path
from os.path import abspath
import time
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.expected_conditions import staleness_of
from selenium.webdriver.common.action_chains import ActionChains
from settings.environment_general_settings import Settings as Conf
from utils.DataEncryptedUtil import DataEncrypted as RandData
from features.factory.validation.exception.RunningException import RunningException as Rexc
from utils.StringsUtil import StringUtil as String
from features.factory.validation.assertion.assertion import Assertion as Assert


ELEMENT_LOCATION = "/html/body"
SEP = os.sep


class Web(object):
    __TIMEOUT_SECS = int(Conf.get_browser_timeout())

    def __init__(self, web_driver):
        super(Web, self).__init__()
        self._web_driver_wait = WebDriverWait(web_driver, Web.__TIMEOUT_SECS)
        self._web_driver = web_driver

    """
         ASSERTIONS
    """
    @staticmethod
    def assert_equals_to(found, expected):
        Assert.equals_to_expected_and_found_value(found, expected)

    @staticmethod
    def assert_equals_to_ignoring_case(found, expected):
        Assert.equals_to_expected_and_found_value(found, expected)

    @staticmethod
    def assert_contains_string(found, expected):
        Assert.contains_expected_in_a_string_label(found, expected)

    @staticmethod
    def assert_contains_string_in_list(found, list_expected):
        Assert.contains_expected_value_in_a_list(found, list_expected)

    @staticmethod
    def assert_value_is_on_interval(item, expected_min, expected_max):
        Assert.contains_expected_value_in_a_interval(item, expected_min, expected_max)

    @staticmethod
    def assert_value_is_equals_to_first_item(item, list_expected):
        Assert.equals_to_expected_and_first_item_on_a_list(item, list_expected)

    """
        BASE SELENIUM METHODS
    """
    def get_web_driver(self):
        return self._web_driver

    def execute_javascript(self, script, *args):
        return self._web_driver.execute_script(script, *args)

    def open(self, _url):
        self.get_web_driver().get(_url)

    def delete_cookies(self):
        print("Deleting browser cookies...")
        self.get_web_driver().delete_all_cookies()

    def maximize_window(self):
        self.get_web_driver().maximize_window()

    def close_and_quit(self):
        self.get_web_driver().quit()

    def take_screenshot(self, file_name):
        scr_file_name = RandData.generate_random_data(10)
        Assert.save_screenshot_in_allure_report(file_name + "_" + scr_file_name, self.get_web_driver())

    def switch_nested_frame(self, by_type, frame_element_location):
        self.wait_until_element_is_present(by_type, frame_element_location)
        frame_element = self.get_element_by(by_type, frame_element_location)
        self.get_web_driver().switch_to.frame(frame_element)

    def switch_to_default_content(self):
        try:
            self.wait_until(1)
            self.get_web_driver().switch_to.default_content()
            self.wait_until(1)
        except RuntimeError as re:
            Rexc.with_raising_error("Element was possible to locate a default content!", re)

    def find_by(self, by_type, element_location):
        try:
            self.wait_until_element_is_present(by_type, element_location)
            element = self.get_web_driver().find_element(by_type, element_location)
            if element.is_displayed() or element.is_enabled() or element.is_selected():
                return element
        except RuntimeError as re:
            Rexc.with_raising_error("Element was not found!", re)

    def find_elements_by(self, by_type, element_location):
        try:
            self.wait_until_elements_are_present(by_type, element_location)
            return self.get_web_driver().find_elements(by_type, element_location)
        except RuntimeError as re:
            Rexc.with_raising_error("Elements were not found!", re)

    """
        GET ELEMENT METHODS
    """
    def get_element_by(self, by_type, element_location):
        return self.find_by(by_type, element_location)

    def get_elements_by(self, by_type, element_location):
        return self.find_elements_by(by_type, element_location)

    """
        WAITING METHODS
    """
    def wait_for_page_load(self):
        old_page = self.find_by(By.XPATH, ELEMENT_LOCATION)
        yield
        WebDriverWait(self.get_web_driver(), 20).until(staleness_of(old_page))

    def wait_until_element_is_clickable(self, by_type, element_location):
        try:
            self._web_driver_wait.until(ec.element_to_be_clickable((by_type, str(element_location))))
        except TimeoutError as te:
            Rexc.with_raising_error("Timed out waiting for a clickable element: ", te)

    def wait_until_element_is_visible(self, by_type, element_location):
        try:
            self._web_driver_wait.until(ec.visibility_of_element_located((by_type, str(element_location))))
        except TimeoutError as te:
            Rexc.with_raising_error("Timed out waiting for a visible element: ", te)

    def wait_until_element_is_present(self, by_type, element_location):
        try:
            self._web_driver_wait.until(ec.presence_of_element_located((by_type, str(element_location))))
        except TimeoutError as te:
            Rexc.with_raising_error("Timed out waiting for a present element: ", te)

    def wait_until_elements_are_present(self, by_type, elements_location):
        try:
            self._web_driver_wait.until(ec.presence_of_all_elements_located((by_type, str(elements_location))))
        except TimeoutError as te:
            Rexc.with_raising_error("Timed out waiting for all elements present: ", te)

    def wait_until_alert_is_present(self):
        try:
            self._web_driver_wait.until(ec.alert_is_present())
        except TimeoutError as te:
            Rexc.with_raising_error("Timed out waiting for page to load: ", te)

    def wait_until(self, secs):
        try:
            WebDriverWait(self.get_web_driver(), secs)
        except TimeoutError as te:
            Rexc.with_raising_error(f"Timed out waiting {secs} seconds: ", te)

    def wait_staleness_by_locator(self, xpath_locator):
        try:
            old_page = self.find_by(By.XPATH, xpath_locator)
            yield
            WebDriverWait(self.get_web_driver(), 20).until(staleness_of(old_page))
        except TimeoutError as te:
            Rexc.with_raising_error(f"Timed out waiting for the element: ", te)

    def wait_staleness_by_element(self, by_type, element_location):
        try:
            element = self.get_element_by(by_type, element_location)
            yield
            WebDriverWait(self.get_web_driver(), 20).until(staleness_of(element))
        except TimeoutError as te:
            Rexc.with_raising_error(f"Timed out waiting for the element: ", te)

    """
        ALERT METHODS
    """
    def accept_alert(self):
        try:
            self.wait_until_alert_is_present()
            alert = self.get_web_driver().switch_to.alert
            if alert is not None:
                alert.accept()
                print("Alert accepted!")
        except:
            pass

    def cancel_alert(self):
        try:
            self.wait_until_alert_is_present()
            alert = self.get_web_driver().switch_to.alert
            if alert is not None:
                alert.dismiss()
                print("Alert canceled!")
        except:
            pass

    """
        CLICK ELEMENT METHODS
    """
    def click_on_element_by(self, by_type, element_location):
        self.wait_until_element_is_present(by_type, element_location)
        self.wait_until_element_is_visible(by_type, element_location)
        self.wait_until_element_is_clickable(by_type, element_location)
        self.get_element_by(by_type, element_location).click()

    def click_on_element_with_name_by(self, value, by_type, element_location):
        self.get_web_driver()
        element = self.get_element_by(by_type, element_location)
        label = element.text
        self.assert_equals_to_ignoring_case(value, label)
        self.get_element_by(by_type, element_location).click()

    def click_on_locator_using_javascript(self, by_type, element_location):
        self.get_web_driver()
        try:
            element = self.get_element_by(by_type, element_location)
            self.get_web_driver().execute_script("arguments[0].click();", element)
        except ValueError as ve:
            Rexc.with_raising_error("This element was not clicked using javascript! ", ve)

    def click_using_javascript_and_coordinates(self, x, y):
        try:
            self.get_web_driver()\
                .execute_script(f"self.driver.execute_script('el=document.elementFromPoint({x},{y});el.click();')")
        except ValueError as ve:
            Rexc.with_raising_error("This element was not clicked using javascript! ", ve)

    """
        GET LABEL METHODS
    """
    def get_label_or_text_by(self, value, by_type, element_location):
        self.get_web_driver()
        try:
            element = self.get_element_by(by_type, element_location)
            label = element.text
            self.assert_equals_to_ignoring_case(label, value)
            return label
        except ValueError as ve:
            Rexc.with_raising_error("This element does not match the text value expected! ", ve)

    def get_label_on_attr(self, attr_name, value, by_type, element_location):
        self.get_web_driver()
        try:
            element = self.get_element_by(by_type, element_location)
            label = element.get_attribute(str(attr_name))
            self.assert_equals_to_ignoring_case(label, value)
            return label
        except ValueError as ve:
            Rexc.with_raising_error("This element does not match the text value expected! ", ve)

    def get_label_element(self, by_type, element_location):
        self.get_web_driver()
        try:
            element = self.get_element_by(by_type, element_location)
            label = str(element.text).strip()
            return label
        except ValueError as ve:
            Rexc.with_raising_error("This element does not match the text value expected! ", ve)

    def get_label_in_text(self, value, by_type, element_location):
        self.get_web_driver()
        try:
            element = self.get_element_by(by_type, element_location)
            label = element.text
            self.assert_contains_string(label, value)
            return label
        except ValueError as ve:
            Rexc.with_raising_error("This element does not match the text value expected! ", ve)

    def get_text_in_label(self, large_text_value, by_type, element_location):
        self.get_web_driver()
        try:
            element = self.get_element_by(by_type, element_location)
            label = element.text
            self.assert_contains_string(large_text_value, label)
            return label
        except ValueError as ve:
            Rexc.with_raising_error("This element does not match the text value expected! ", ve)

    """
        TYPING TEXT METHODS
    """
    def type_text_by(self, value, by_type, element_location):
        self.get_web_driver()
        try:
            element = self.get_element_by(by_type, element_location)
            if String.is_not_blank_or_null(value):
                element.clear()
                element.send_keys(value)
        except ValueError as ve:
            Rexc.with_raising_error("This element is not iterable or value not matches! ", ve)

    """
        KEYS PRESS METHODS
    """
    def press_key_down(self, multiple_times):
        for i in range(0, multiple_times):
            self.get_element_by(By.XPATH, ELEMENT_LOCATION).send_keys(Keys.ARROW_DOWN)
        self.wait_staleness_by_locator(ELEMENT_LOCATION)
        self.switch_to_default_content()

    def press_key_up(self, multiple_times):
        for i in range(0, multiple_times):
            self.get_element_by(By.XPATH, ELEMENT_LOCATION).send_keys(Keys.ARROW_UP)
        self.wait_staleness_by_locator(ELEMENT_LOCATION)
        self.switch_to_default_content()

    def press_key_page_down(self):
        self.get_element_by(By.XPATH, ELEMENT_LOCATION).send_keys(Keys.PAGE_DOWN)

    def press_key_page_up(self):
        self.get_element_by(By.XPATH, ELEMENT_LOCATION).send_keys(Keys.PAGE_UP)

    def press_key_home(self):
        self.get_element_by(By.XPATH, ELEMENT_LOCATION).send_keys(Keys.HOME)

    def press_key_end(self):
        self.get_element_by(By.XPATH, ELEMENT_LOCATION).send_keys(Keys.END)

    def press_key_tab(self):
        self.get_element_by(By.XPATH, ELEMENT_LOCATION).send_keys(Keys.TAB)

    def press_key_space(self):
        self.get_element_by(By.XPATH, ELEMENT_LOCATION).send_keys(Keys.SPACE)

    """
        TYPING CHECKBOX METHODS
    """
    def type_check_option_by_label(self, label_option, by_type, element_location_with_sibling):
        try:
            self.wait_until_element_is_clickable(by_type, element_location_with_sibling)
            if String.is_not_blank_or_null(label_option):
                element = self.get_element_by(By.XPATH, element_location_with_sibling)
                label_result = element.text
                Assert.equals_to_expected_and_found_value(label_result, label_option)
            if str(element_location_with_sibling).__contains__("following-sibling::"):
                checkbox_element = self.get_element_by(By.XPATH, str(element_location_with_sibling).split("following-sibling::")[0])
                checkbox_element.click()
            else:
                checkbox_element = self.get_element_by(By.XPATH, str(element_location_with_sibling))
                checkbox_element.click()
        except RuntimeError as re:
            Rexc.with_raising_error("Element was not able to be selected! ", re)

    def type_select_combo_option(self, option, by_type, element_location):
        select = Select(self.get_element_by(by_type, element_location))
        try:
            index_option = int(option)
            if index_option >= 0:
                select.select_by_index(index_option)
            else:
                select.select_by_value(index_option)
        except:
            if String.is_not_blank_or_null(option):
                select.select_by_visible_text(option)

    """
        BASIC ACTION CHAINS METHODS
    """
    def perform_move_to_element(self, by_type, element_location):
        self.wait_until_element_is_present(by_type, element_location)
        element = self.get_element_by(by_type, element_location)
        return ActionChains(self.get_web_driver()).move_to_element(element)

    def perform_move_to_element_with_offset(self, x, y, by_type, element_location):
        self.wait_until_element_is_present(by_type, element_location)
        element = self.get_element_by(by_type, element_location)
        return ActionChains(self.get_web_driver()).move_to_element_with_offset(element, int(x), int(y))

    def perform_move_to_element_releasing(self, by_type, element_location):
        self.wait_until_element_is_present(by_type, element_location)
        element = self.get_element_by(by_type, element_location)
        return ActionChains(self.get_web_driver()).move_to_element(element).release(element).perform()

    def perform_send_keys_to_element_action(self, by_type, element_location):
        self.perform_move_to_element(by_type, element_location).send_keys_to_element().perform()

    def perform_reset_action(self, by_type, element_location):
        self.perform_move_to_element(by_type, element_location).reset_actions()

    def perform_click_action(self, by_type, element_location):
        self.wait_until_element_is_present(by_type, element_location)
        self.wait_until_element_is_visible(by_type, element_location)
        self.wait_until_element_is_clickable(by_type, element_location)
        self.perform_move_to_element(by_type, element_location).click().perform()

    def perform_double_click_action(self, by_type, element_location):
        self.wait_until_element_is_clickable(by_type, element_location)
        self.perform_move_to_element(by_type, element_location).double_click().perform()

    def perform_context_click_action(self, by_type, element_location):
        self.wait_until_element_is_clickable(by_type, element_location)
        self.perform_move_to_element(by_type, element_location).context_click().perform()

    def perform_click_and_hold_action(self, by_type, element_location):
        self.wait_until_element_is_clickable(by_type, element_location)
        self.perform_move_to_element(by_type, element_location).click_and_hold().perform()

    def perform_click_action_with_offset(self, by_type, element_location, x, y):
        self.wait_until_element_is_present(by_type, element_location)
        self.wait_until_element_is_visible(by_type, element_location)
        self.perform_move_to_element_with_offset(x, y, by_type, element_location).click().perform()

    def perform_drag_and_drop_action(self, by_type, element_location):
        self.wait_until_element_is_clickable(by_type, element_location)
        self.perform_move_to_element(by_type, element_location).drag_and_drop().perform()

    def perform_drag_and_drop_with_offset_action(self, by_type, element_location, x, y):
        element = self.get_element_by(by_type, element_location)
        self.wait_until_element_is_clickable(by_type, element_location)
        action_chains = ActionChains(self.get_web_driver())
        return action_chains.drag_and_drop_by_offset(element, x, y)\
            .click_and_hold(element)\
            .pause(3)\
            .release(element)\
            .perform()

    def perform_move_by_offset_action(self, by_type, element_location, x, y):
            element = self.get_element_by(by_type, element_location)
            self.wait_until_element_is_present(by_type, element_location)
            self.wait_until_element_is_visible(by_type, element_location)
            self.wait_until_element_is_clickable(by_type, element_location)
            action_chains = ActionChains(self.get_web_driver())
            action_chains.click_and_hold(element).move_by_offset(x, y).release(element).pause(1).perform()
            self.wait_staleness_by_locator(element_location)

    def slider_offset_end_to_start(self, end, discretization, value_expected, *element_located):
        value = int(value_expected)
        end = int(end)
        if value < end:
            quota = 16 * ((end - int(value)) / int(discretization))
            total_moving_x = abs(round(quota))
            self.perform_move_by_offset_action(*element_located, -total_moving_x, 0)
        else:
            pass

    def slider_offset_start_to_end(self, start, discretization, value_expected, *element_located):
        value = int(value_expected)
        start = int(start)
        if value > start:
            quota = 16 * ((start - int(value)) / int(discretization))
            total_moving_x = abs(round(quota))
            self.perform_move_by_offset_action(*element_located, total_moving_x, 0)
        else:
            pass

    def upload_with_drag_and_drop(self, path_to_upload, by_drop, drop_locator):
        self.get_web_driver()
        try:
            abs_file_path = abspath(path_to_upload)
            element_to_drop = self.get_element_by(by_drop, drop_locator)
            self.get_web_driver().execute_script(
                'arguments[0].style = ""; arguments[0].style.display = "block"; arguments[0].style.visibility = "visible";',
                element_to_drop)
            element_to_drop.send_keys(abs_file_path)
        except Exception as ex:
            Rexc.with_raising_error("It was not possible to perform uploading! ", ex)
