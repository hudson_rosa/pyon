import json
import time
import logging
from utils.TextColorUtil import TextColor as Color
from utils.StringsUtil import StringUtil as StrUtil
from settings.environment_general_settings import Settings as Conf
from features.factory.validation.exception.RunningException import RunningException as Rexc

logger = logging.getLogger(__name__)


class BaseLogging:

    @staticmethod
    def iteration_number(i):
        print("-->  Iteration number: " + str(i))

    @staticmethod
    def waiting_for(time_wait):
        print("--> Waiting for " + str(time_wait) + " second(s)...")
        time.sleep(time_wait)

    @staticmethod
    def log_status_exception(exc, response_status):
        error_status_message = "\n>>> Request failed! STATUS ERROR: <<<"
        end_stacktrace_message = ">>> End Stacktrace <<<"
        if exc is None:
            print(Color.red(error_status_message))
            print(Color.red(">>> " + response_status))
            print(Color.red(end_stacktrace_message))
            raise Exception(response_status)
        else:
            print(Color.red(error_status_message))
            print(Color.red(">>> " + response_status))
            print(Color.red(exc.args))
            print(Color.red(end_stacktrace_message))
            raise Exception(response_status)

    @staticmethod
    def log_status_failed(response, exc, fail_message):
        if Conf.get_api_response_body() == "False".lower().strip():
            parsed_body = json.loads(str(response.text))
            print("\n-- HEADER BODY --")
            print(response.headers)
            print("\n-- RESPONSE BODY --")
            print(StrUtil.indent_dict_body(parsed_body))
        BaseLogging.log_assert_message(response.status_code, Color.red(fail_message))
        BaseLogging.log_status_exception(exc, Color.red(response.status_code))

    @staticmethod
    def log_status_passed(response, success_message):
        if response is None:
            pass
        else:
            BaseLogging.log_assert_message(response.status_code, Color.green(success_message))

    @staticmethod
    def log_assert_message(response_status, message):
        if response_status is None:
            print("\n--> Assertion) " + message + " <--")
        else:
            print("\n--> Assertion) Status " + str(response_status) + ": " + message + " <--")

    @staticmethod
    def log_short_status_passed():
        print(Color.green(">>>>>>> STEP PASSED! <<<<<<<\n"))

    @staticmethod
    def log_short_status_failed():
        print(Color.red(">>>>>>> STEP FAILED! <<<<<<<\n"))

    @staticmethod
    def log_response_body(response):
        try:
            if Conf.get_api_response_body() == "True".lower().strip():
                parsed_body = json.loads(str(response.text))
                print("\n-- RESPONSE BODY --")
                print(StrUtil.indent_dict_body(parsed_body))
        except Exception as ex:
            Rexc.with_raising_error("Impossible to get the response! ", ex)

    @staticmethod
    def log_header_body(response):
        try:
            if Conf.get_api_response_body() == "True".lower().strip():
                print("\n-- HEADER BODY --")
                print(response.headers)
        except Exception as ex:
            Rexc.with_raising_error("Impossible to get the response ", ex)

    @staticmethod
    def log_url_for_test(url, auth_key, token):
        print(Color.blue(f"###### URL: {url} ######"))
        print(Color.blue(f"###### {auth_key}: {token} ######"))
        print(StrUtil.get_marker_from_a_text_length(url, "-"))

    def log_test_name(self):
        print("\n")
        print(StrUtil.get_marker_from_a_text_length("---------------------------" + str(self), "-"))
        print(Color.yellow("###### METHOD USED: " + str(self) + " ######"))
        print(StrUtil.get_marker_from_a_text_length("---------------------------" + str(self), "-"))
