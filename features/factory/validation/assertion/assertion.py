import os
import allure
from allure_commons.types import AttachmentType
from utils.TextColorUtil import TextColor as Color
from settings.environment_general_settings import Settings as Conf
from features.factory.validation.exception.RunningException import RunningException as Rexc


SEP = os.sep


class Assertion:

    @staticmethod
    def equals_to_expected_and_found_value(found, expected_lower):
        label_lower = str(found).lower().strip()
        expected_lower = str(expected_lower).lower().strip()
        try:
            assert expected_lower == label_lower
            if label_lower == expected_lower:
                print(f"(\u2713) {expected_lower} - Found: {label_lower}")
        except:
            Rexc.with_message(f"(X) {expected_lower}, but Found: {label_lower}")

    @staticmethod
    def contains_expected_in_a_string_label(large_found, expected):
        large_found = str(large_found).strip()
        expected = str(expected).strip()
        try:
            assert expected in large_found
            if expected in large_found:
                print(f"(\u2713) {expected} is in Found: {large_found}")
        except:
            Rexc.with_message(f"(X) {expected}, but not is in Found: {large_found}")

    @staticmethod
    def contains_expected_value_in_a_list(found, list_expected):
        found = str(found).strip()
        assert found in list_expected
        try:
            if found in list_expected:
                print(f"(\u2713) Expected: {str(list_expected)} contains Found: {found}")
        except:
            Rexc.with_message(f"(X) Expected: {str(list_expected)}, but not contains Found: {found}")

    @staticmethod
    def contains_expected_value_in_a_interval(item_found, expected_min, expected_max):
        item = int(item_found)
        max_no = int(expected_max)
        min_no = int(expected_min)
        assert min_no <= item <= max_no
        try:
            if min_no <= item <= max_no:
                print(f"(\u2713) {item} is between {min_no} and {max_no}")
        except:
            Rexc.with_message(f"(X) {item} is not between {min_no} and {max_no}")

    @staticmethod
    def equals_to_expected_and_first_item_on_a_list(item_found, list_expected):
        found = str(item_found).strip()
        assert found == list_expected[0]
        try:
            if found == list_expected[0]:
                print(f"(\u2713) Expected: First item {str(list_expected[0])} is equals to Found: {found}")
        except:
            Rexc.with_message(f"(X) Expected: First item {str(list_expected[0])}, but is not equals to Found: {found}")


    @staticmethod
    def save_screenshot_in_allure_report(file_name, web_driver):
        scr_path = f"{Conf.get_screenshot_path}{SEP}{file_name}.png"
        web_driver.save_screenshot(scr_path)
        allure.attach(web_driver.get_screenshot_as_png(),
                      f"{file_name}",
                      attachment_type=AttachmentType.PNG)
        print(Color.red(f"Screenshot taken! --> {scr_path}"))
