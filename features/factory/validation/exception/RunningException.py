from utils.TextColorUtil import TextColor as Color


class RunningException:

    @staticmethod
    def with_raising_error(custom_message, exception):
        message_body = Color.red(f"\n{custom_message} ---- {exception}\n")
        print("------------------------------------")
        print(message_body)
        print("------------------------------------")
        raise Exception(message_body)

    @staticmethod
    def with_message(custom_message):
        print(custom_message)
        raise Exception(custom_message)
