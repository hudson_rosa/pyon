import time
from settings.environment_general_settings import Settings as Conf
from utils.DataEncryptedUtil import DataEncrypted as RandData
from features.factory.validation.exception.RunningException import RunningException as Rexc
from utils.StringsUtil import StringUtil as String
from features.factory.validation.assertion.assertion import Assertion as ValUtil


class Mob(object):
    __TIMEOUT_SECS = int(Conf.get_browser_timeout())

    def __init__(self, mob_driver):
        super(Mob, self).__init__()
        self._mob_driver = mob_driver
        self._mob_driver_wait = self._mob_driver.implicitly_wait(Mob.__TIMEOUT_SECS)
        # from appium import webdriver
        # desired_caps = {}
        # self._mob_driver = webdriver.Remote(Conf.get_mob_remote_endpoint(), desired_caps)

    """
        BASE APPIUM METHODS
    """
    def get_mob_driver(self):
        return self._mob_driver

    def open(self, _url):
        self.get_mob_driver().get(_url)

    def delete_cookies(self):
        print("Deleting browser cookies...")
        self.get_mob_driver().delete_all_cookies()

    def close_and_quit(self):
        self.get_mob_driver().quit()

    def take_screenshot(self, file_name):
        scr_file_name = RandData.generate_random_data(10)
        ValUtil.save_screenshot_in_allure_report(file_name + "_MOB_" + scr_file_name, self.get_mob_driver())

    def force_sleep(self, seconds):
        time.sleep(seconds)

    def find_by(self, element_location):
        try:
            return self.get_mob_driver().find_element(element_location)
        except RuntimeError as re:
            Rexc.with_raising_error("Element was not found!", re)

    def find_by_accessibility_id(self, element_location):
        try:
            return self.get_mob_driver().find_element_by_accessibility_id(element_location)
        except RuntimeError as re:
            Rexc.with_raising_error("Element was not found!", re)

    def find_by_xpath(self, element_location):
        try:
            return self.get_mob_driver().find_element_by_xpath(element_location)
        except RuntimeError as re:
            Rexc.with_raising_error("Element was not found!", re)

    def find_by_name(self, element_location):
        try:
            return self.get_mob_driver().find_element_by_name(element_location)
        except RuntimeError as re:
            Rexc.with_raising_error("Element was not found!", re)

    def find_by_android_uiautomator(self, element_location):
        try:
            return self.get_mob_driver().find_element_by_android_uiautomator(element_location)
        except RuntimeError as re:
            Rexc.with_raising_error("Element was not found!", re)

    def find_elements_by(self, element_location):
        return self.get_mob_driver().find_elements(element_location)

    """
        GET ELEMENT METHODS
    """
    def get_element_by(self, element_location):
        return self.find_by(element_location)

    def get_element_by_accessibility_id(self, element_location):
        return self.find_by_accessibility_id(element_location)

    def get_element_by_xpath(self, element_location):
        return self.find_by_xpath(element_location)

    def get_element_by_name(self, element_location):
        return self.find_by_name(element_location)

    def get_element_by_android_uiautomator(self, element_location):
        return self.find_by_android_uiautomator('new UiSelector().'+element_location)

    """
        CLICK ELEMENT METHODS
    """
    def click_on_element_by(self, element_location):
        self.get_element_by(element_location).click()

    def click_on_element_by_accessibility_id(self, element_location):
        self.get_element_by_accessibility_id(element_location).click()

    def click_on_element_by_xpath(self, element_location):
        self.get_element_by_xpath(element_location).click()

    def click_on_element_by_name(self, element_location):
        self.get_element_by_name(element_location).click()

    def click_on_element_by_android_uiautomator(self, element_location):
        self.get_element_by_android_uiautomator('new UiSelector().'+element_location).click()


    """
        GET LABEL METHODS
    """
    def get_label_or_text_by(self, value, element_location):
        self.get_mob_driver()
        try:
            element = self.get_element_by(element_location)
            label = element.text
            ValUtil.equals_to_expected_and_found_value(label, value)
            if String.is_not_blank_or_null(value):
                label = str(label).strip()
                assert str(value).__eq__(label)
                return label
        except ValueError as ve:
            self.display_message_element_value_error(ve)

    def get_label_or_text_by_xpath(self, value, element_location):
        self.get_mob_driver()
        try:
            element = self.get_element_by_xpath(element_location)
            label = element.text
            ValUtil.equals_to_expected_and_found_value(label, value)
            if String.is_not_blank_or_null(value):
                label = str(label).strip()
                assert str(value).__eq__(label)
                return label
        except ValueError as ve:
            self.display_message_element_value_error(ve)

    def get_label_or_text_by_android_uiautomator(self, value, element_location):
        self.get_mob_driver()
        try:
            element = self.get_element_by_android_uiautomator(element_location)
            label = element.text
            ValUtil.equals_to_expected_and_found_value(label, value)
            if String.is_not_blank_or_null(value):
                label = str(label).strip()
                assert str(value).__eq__(label)
                return label
        except ValueError as ve:
            self.display_message_element_value_error(ve)

    """
        TYPING ELEMENT METHODS
    """
    def type_text_by(self, value, element_location):
        self.get_mob_driver()
        try:
            element = self.get_element_by(element_location)
            if String.is_not_blank_or_null(value):
                element.clear()
                element.send_keys(value)
        except ValueError as ve:
            self.display_element_not_found_message(ve)

    def type_text_by_accessibility_id(self, value, element_location):
        self.get_mob_driver()
        try:
            element = self.get_element_by_accessibility_id(element_location)
            if String.is_not_blank_or_null(value):
                element.clear()
                element.send_keys(value)
        except ValueError as ve:
            self.display_element_not_found_message(ve)

    def type_text_by_xpath(self, value, element_location):
        self.get_mob_driver()
        try:
            element = self.get_element_by_xpath(element_location)
            if String.is_not_blank_or_null(value):
                element.clear()
                element.send_keys(value)
        except ValueError as ve:
            self.display_element_not_found_message(ve)

    def type_text_by_name(self, value, element_location):
        self.get_mob_driver()
        try:
            element = self.get_element_by_name(element_location)
            if String.is_not_blank_or_null(value):
                element.clear()
                element.send_keys(value)
        except ValueError as ve:
            self.display_element_not_found_message(ve)

    def type_text_by_android_uiautomator(self, value, element_location):
        self.get_mob_driver()
        try:
            element = self.get_element_by_android_uiautomator(element_location)
            if String.is_not_blank_or_null(value):
                element.clear()
                element.send_keys(value)
        except ValueError as ve:
            self.display_element_not_found_message(ve)

    @staticmethod
    def display_element_not_found_message(ve):
        Rexc.with_raising_error("This element is not iterable or value not matches! ", ve)

    @staticmethod
    def display_message_element_value_error(ve):
        Rexc.with_raising_error("This element does not matches the text value expected! ", ve)

