import os, sys
from appium import webdriver as mobdriver
from settings.environment_general_settings import Settings as Conf
from features.factory.validation.exception.RunningException import RunningException as Rexc


PATH = lambda p: os.path.abspath(os.path.join(os.path.dirname(__file__), p))


class MobFactory:

    @staticmethod
    def get_mobile():
        is_mob_enabled = Conf.get_mobile_test_enabled()
        device_farms_name = Conf.get_device_farms_name()
        device_orientation = Conf.get_mobile_device_orientation()
        platform_name = Conf.get_mobile_platform_name()
        platform_version = Conf.get_mobile_platform_version()
        device_name = Conf.get_mobile_device_name()
        browser_name = Conf.get_mobile_browser_name()
        automation_name = Conf.get_mobile_automation_name()
        app_path = Conf.get_mob_app_path()
        app_activity = Conf.get_mob_app_activity()
        mobile_endpoint = Conf.get_device_farm_endpoint()
        mobile_udid = Conf.get_mobile_udid()
        user_device_farm = Conf.get_device_farm_user_key()
        access_key_device_farm = Conf.get_device_farm_access_key()
        if is_mob_enabled == "true" and "saucelabs" in device_farms_name:
            caps = {
                'browserName': browser_name,
                'deviceName': device_name,
                'platformName': platform_name,
                'platformVersion': platform_version,
                'automationName': automation_name,
                'deviceOrientation': device_orientation,
                'app': PATH(app_path)
            }
            user = os.environ.get('SAUCE_USERNAME', user_device_farm)
            access_key = os.environ.get('SAUCE_ACCESS_KEY', access_key_device_farm)
            url = mobile_endpoint.format(user, access_key)
            driver = mobdriver.Remote(url, desired_capabilities=caps)
            MobFactory.log_custom_driver(platform_name, platform_version, device_name)
            return driver
        if is_mob_enabled == "true" and "browserstack" in device_farms_name:
            app_id = os.environ['MOB_APP_ID']
            user = os.environ.get('BROWSERSTACK_USERNAME', user_device_farm)
            access_key = os.environ.get('BROWSERSTACK_ACCESS_KEY', access_key_device_farm)
            caps = {
                'browserName': browser_name,
                'name': automation_name,
                'device': device_name,
                'realMobile': 'true',
                'os_version': platform_version,
                'app': f'{app_id}',
                'browserstack.debug': 'false',
                'browserstack.local': 'false'
            }
            driver = mobdriver.Remote(
                command_executor=f'https://{user}:{access_key}@{mobile_endpoint}', desired_capabilities=caps)
            MobFactory.log_custom_driver(platform_name, platform_version, device_name)
            return driver
        if is_mob_enabled == "true" and "local" in device_farms_name:
            app_id = os.environ['MOB_APP_ID']
            url = "http://localhost:4723/wd/hub"
            caps = {
                'browserName': browser_name,
                'deviceName': device_name,
                'platformName': platform_name,
                'platformVersion': platform_version,
                # 'automationName': automation_name,
                'deviceOrientation': device_orientation,
                'deviceId': f'{app_id}',
                'app': os.path.abspath(app_path),
                'udid': mobile_udid,
                'appActivity': app_activity,
                'allowTestPackages': 'true',
                'connectHardwareKeyboard': 'true',
                'noReset': 'true',
                'fullReset': 'false'
            }
            driver = mobdriver.Remote(url, desired_capabilities=caps, direct_connection=False)
            MobFactory.log_custom_driver(platform_name, platform_version, device_name)
            return driver

    @staticmethod
    def check_mob_driver_dir():
        if os.path.exists(Conf.get_mob_driver_dir()):
            return Conf.get_mob_driver_dir()
        else:
            return Conf.get_resources_path_mobile_apps()

    @staticmethod
    def get_os_platform_for_file_format():
        if os.name == 'nt':
            return ".exe"
        else:
            return ""

    @staticmethod
    def log_custom_driver(platform, version, device):
        print(str(platform+' '+version).upper() + f" is running in {device}...")

    @staticmethod
    def cleanup_driver(driver):
        if driver is not None:
            driver.quit()

    @staticmethod
    def force_kill_adb_process():
        try:
            if Conf.get_mobile_test_enabled() == "true":
                os.system(f'adb disconnect')
        except:
            Rexc.with_message("None ADB found in your OS!")
