@fixture.web.chrome @web_test
Feature: Choosing and sending a product to cart in Automation Practice website

	Background: GET the Automation Practice Website
		Given a URL from "http://automationpractice.com/index.php" website

	Scenario: The user can search a product and send to the chart
		Given the user is on the home page
		When the user type the "Blouse" word on search
		And the user clicks on the search button
		And the product is found after search
		And the user sees the unit price "$27.00"
		And the user clicks on "Add to cart" button
		And the user clicks on "Proceed to checkout" button
		Then checks if the total products purchased is the same of unit price
		And the total shipping is "$2.00"
		But the total of purchasing is "$29.00"
