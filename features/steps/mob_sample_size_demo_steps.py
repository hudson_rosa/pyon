from behave import *
from features.pages.sample_size_demo.formulas_page import FormulasPage
from features.pages.sample_size_demo.home_page import HomePage

# SCENARIO
@given('I click on Formulas tab')
def step_impl(context):
    FormulasPage.get_formulas_tab(context.mob)
    FormulasPage.click_formulas_tab(context.mob)


@given('I click on Home tab')
def step_impl(context):
    HomePage.click_home_tab(context.mob)


@when('I focus and type "{_number}" on Population field')
def step_impl(context, _number):
    HomePage.type_text_population(context.mob, _number)


@then('I see the value {_value} calculated')
def step_impl(context, _value):
    HomePage.get_calculation(context.mob, _value)
