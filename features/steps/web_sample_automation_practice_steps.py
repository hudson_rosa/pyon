from behave import *
from features.pages.automation_practice_website.home_page import HomePage
from features.pages.automation_practice_website.modal_cart_page import ModalCartPage
from features.pages.automation_practice_website.shopping_cart_summary_page import CartSummaryPage


# BACKGROUND
@given('a URL from "{website}" website')
def step_impl(context, website):
    context.website = website

# SCENARIO
@given('the user is on the home page')
def step_impl(context):
    context.web.open(context.website)


@when('the user type the "{clothing_name}" word on search')
def step_impl(context, clothing_name):
    context.clothing = clothing_name
    context.web.type_text_by(context.clothing, *HomePage.loc_text_search)


@when('the user clicks on the search button')
def step_impl(context):
    context.web.click_on_element_by(*HomePage.loc_button_search)


@when('the product is found after search')
def step_impl(context):
    context.web.wait_until_element_is_visible(*HomePage.loc_label_product_name)
    context.web.press_key_page_down()
    context.web.get_label_or_text_by(context.clothing, *HomePage.loc_label_product_name)


@when('the user sees the unit price "{unit_price}"')
def step_impl(context, unit_price):
    context.unit_price = unit_price
    context.web.get_label_or_text_by(context.unit_price, *HomePage.loc_label_product_unit_price)


@when('the user clicks on "Add to cart" button')
def step_impl(context):
    context.web.perform_move_to_element_releasing(*HomePage.loc_hover_product)
    context.web.perform_click_action(*HomePage.loc_button_add_to_cart)


@when('the user clicks on "Proceed to checkout" button')
def step_impl(context):
    context.web.wait_until_element_is_visible(*ModalCartPage.loc_modal_cart_adding)
    context.web.perform_move_to_element_releasing(*ModalCartPage.loc_modal_cart_adding)
    context.web.wait_until_element_is_visible(*ModalCartPage.loc_button_proceed_to_checkout)
    context.web.perform_click_action(*ModalCartPage.loc_button_proceed_to_checkout)


@then('checks if the total products purchased is the same of unit price')
def step_impl(context):
    # This is an example using the method created in page object
    CartSummaryPage.get_label_total_product(context.web, context.unit_price)


@then('the total shipping is "{total_shipping}"')
def step_impl(context, total_shipping):
    CartSummaryPage.get_label_total_shipping(context.web, total_shipping)


@then('the total of purchasing is "{total}"')
def step_impl(context, total):
    CartSummaryPage.get_label_total_price(context.web, total)


