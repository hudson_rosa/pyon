import json
import requests
from behave import *
from features.factory.api.get_api_impl import GETStep as apiGet
from features.schemas.sample_swapi_planets.get_planets_schema import Schema


# BACKGROUND
@given('a request without an authentication')
def step_impl(context):
    context.auth_id = None
    context.token = None


# HEALTH CHECK SCENARIO
@given('a valid planet as "{_planet}" by ID in the SWAPI')
def step_impl(context, _planet):
    context.id = "14"
    context.planet = _planet


@given('the SWAPI URL supports planet research')
def step_impl(context):
    context.url = "https://swapi.co/api/planets/"


@when('the user sends a GET Http request using the URL with ID')
def step_impl(context):
    context.url_id = context.url + context.id
    context.response = requests.get(context.url_id, verify=False, params="")
    response_status = context.response.status_code
    assert response_status == 200


@then('the response contains the planet expected')
def step_impl(context):
    context.name = "name"
    json_resp = json.loads(context.response.text)
    if context.name in json_resp:
        apiGet.assert_key_contains_value(
            context.url_id, 1, context.auth_id, context.token, context.name, "", context.planet
        )
    else:
        raise Exception


# CONTRACT SCENARIO
@given('a request url "{_url_planet_3}"')
def step_impl(context, _url_planet_3):
    context.url_planet_3 = _url_planet_3


@given('the response with 200 status with registers')
def step_impl(context):
    apiGet.assert_key_contains_value(context.url_planet_3, 1, context.auth_id, context.token, "name", "", "Yavin IV")


@when('the user sets a schema')
def step_impl(context):
    context.schema = Schema.call_get_planets()


@when('sets the response body')
def step_impl(context):
    """
    context.resp_body = [{
            "name": "Kashyyyk",
            "rotation_period": "26",
            "orbital_period": "381",
            "diameter": "12765",
            "climate": "tropical",
            "gravity": "1 standard",
            "terrain": "jungle, forests, lakes, rivers",
            "surface_water": "60",
            "population": "45000000",
            "residents": [
                "https://swapi.co/api/people/13/",
                "https://swapi.co/api/people/80/"
            ],
            "films": [
                "https://swapi.co/api/films/6/"
            ],
            "created": "2014-12-10T13:32:00.124000Z",
            "edited": "2014-12-20T20:58:18.442000Z",
            "url": "https://swapi.co/api/planets/14/"
        }]
    """
    context.resp_body = apiGet.get_response(context.url_planet_3, context.auth_id, context.token)


@then('the json response matches the schema')
def step_impl(context):
    apiGet.assert_json_schema(context.schema, context.resp_body)


# FUNCTIONAL OUTLINE SCENARIO
@given('the valid parameters "{_url}", "{_planet}" and "{_id}" to SWAPI')
def step_impl(context, _url, _planet, _id):
    context.url = _url
    context.planet = _planet
    context.id = _id


@when('the user sends a GET Http request with these parameters')
def step_impl(context):
    context.url_id = context.url + context.id
    apiGet.assert_health_check_is_200(
        context.url_id, 1, context.auth_id, context.token, ""
    )


@then('the response for url contains the planet name')
def step_impl(context):
    context.name = "name"
    context.response = requests.get(context.url_id, verify=False, params="")
    json_resp = json.loads(context.response.text)
    if context.name in json_resp:
        apiGet.assert_key_contains_value(
            context.url_id, 1, context.auth_id, context.token, context.name, "", context.planet
        )
    else:
        raise Exception

# FUNCTIONAL SCENARIO
@given('a new request url "{_url_planet_8}"')
def step_impl(context, _url_planet_8):
    context.url = _url_planet_8


@when('the user defines "{_requests_number}" requests to send in {_minutes} min')
def step_impl(context, _requests_number, _minutes):
    context.req_number = _requests_number
    context.minutes = _minutes


@then('the SWAPI interrupt with 429 status')
def step_impl(context):
    apiGet.assert_x_rate_limit_is_429(context.url, int(context.req_number), float(context.minutes), context.auth_id, context.token, 0, 0)
