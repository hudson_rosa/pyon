import json
from behave import *
from features.queries.sql_sample_connection_test_sqls import ScriptPyonSchema as SqlSample


# CONTEXTO
@given('que o usuário se conecta ao banco de dados')
def step_impl(context):
    db_connection = {
        "db_user": "hudsenberg",
        "db_password": "Huds3nb3rg@qa",
        "db_host": "localhost",
        "db_name": "pyon_schema_tests",
        "db_type": "MySQL",
        "db_driver_abs_path": ""
    }
    context.db_connection = json.dumps(db_connection)


@given('o usuário apaga e recria a base de dados com as instruções SQL')
def step_impl(context):
    SqlSample.query_destroy_database(context.db_connection)
    SqlSample.query_create_database(context.db_connection)


# CENÁRIO
@given('que o usuário cria as tabelas tb_test_resource, tb_test_type, tb_category e tb_feature')
def step_impl(context):
    SqlSample.query_create_tables(context.db_connection)


@when('o usuário preenche estas tabelas')
def step_impl(context):
    SqlSample.query_populate(context.db_connection)


@then('o usuário pode consultar com "join" entre as tabelas tb_feature, tb_test_type e tb_category')
def step_impl(context):
    SqlSample.query_select_feature_categories(context.db_connection)


@then('o resultado deverá apresentar "{_number}" registros de funcionalidades')
def step_impl(context, _number):
    SqlSample.query_select_count_feature_categories(context.db_connection)
