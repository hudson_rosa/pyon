import json
from behave import *
from features.queries.sql_sample_connection_test_sqls import ScriptPyonSchema as SqlSample


# BACKGROUND
@given('the user can connect on database')
def step_impl(context):
    db_connection = {
        "db_user": "hudsenberg",
        "db_password": "Huds3nb3rg@qa",
        "db_host": "localhost",
        "db_name": "pyon_schema_tests",
        "db_type": "MySQL",
        "db_driver_abs_path": ""
    }
    context.db_connection = json.dumps(db_connection)


@given('reset the database with SQL statements')
def step_impl(context):
    SqlSample.query_destroy_database(context.db_connection)
    SqlSample.query_create_database(context.db_connection)


# SCENARIO
@given('the user create tables tb_test_resource, tb_test_type, tb_category and tb_feature')
def step_impl(context):
    SqlSample.query_create_tables(context.db_connection)


@when('the user populates these tables')
def step_impl(context):
    SqlSample.query_populate(context.db_connection)


@then('the user can select with "join" between tb_feature, tb_test_type and tb_category tables')
def step_impl(context):
    SqlSample.query_select_feature_categories(context.db_connection)


@then('the result should show "{_number}" registers of features')
def step_impl(context, _number):
    SqlSample.query_select_count_feature_categories(context.db_connection)
