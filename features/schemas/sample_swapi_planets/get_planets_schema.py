
class Schema:

    @staticmethod
    def call_get_planets():
        return {
            "type": "object",
            "required": ["name", "rotation_period"],
            "properties": {
                "name": {"type": "string"},
                "rotation_period": {"type": "string"},
                "orbital_period": {"type": "string"},
                "diameter": {"type": "string"},
                "climate": {"type": "string"},
                "gravity": {"type": "string"},
                "terrain": {"type": "string"},
                "surface_water": {"type": "string"},
                "population": {"type": "string"},
                "residents": {"type": "array"}, "items": {"type": "string", "format": "url"},
                "films": {"type": "array"}, "items": {"type": "string", "format": "url"},
                "created": {"type": "string", "format": "date-time"},
                "edited": {"type": "string", "format": "date-time"},
                "url": {"type": "string", "format": "url"}
            }
        }
