import os
from behave import fixture
from features.factory.web.web_impl import Web
from features.factory.web.sel_standalone_setup import GridFactory
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium import webdriver
from settings.environment_general_settings import Settings as Conf
from features.factory.validation.exception.RunningException import RunningException as Rexc

GECKODRIVER = "geckodriver"
MICROSOFT_WEB_DRIVER = "MicrosoftWebDriver"
MSEDGEDRIVER = "msedgedriver"
CHROMEDRIVER = "chromedriver"
REMOTE_USERNAME = Conf.get_device_farm_user_key()
REMOTE_PASSWORD = Conf.get_device_farm_access_key()
BROWSERSTACK_URL = f"http://{REMOTE_USERNAME}:{REMOTE_PASSWORD}@hub-cloud.browserstack.com/wd/hub"
DESIRED_CAPS = {'platform': 'ANY',
                'version': '',
                'javascriptEnabled': True}


def log_web_driver_error(ex):
    Rexc.with_raising_error(f"Error to execute the Web Driver. \nPrevious cause: ", ex)


def log_web_driver_in_instance(webdriver_name):
    print(f"Browser {str(webdriver_name).upper()} is running...")


def check_web_driver_dir():
    if os.path.exists(Conf.get_web_driver_dir()):
        return Conf.get_web_driver_dir()
    else:
        return Conf.get_resources_resources_webdriver()


def get_os_platform_to_execute_a_file_format():
    if os.name == 'nt':
        return ".exe"
    else:
        return ""


def provide_webdriver(context, web, wd_name):
    log_web_driver_in_instance(wd_name)
    context.web = Web(web)
    context.web.maximize_window()
    yield context.web
    context.web.delete_cookies()
    context.web.close_and_quit()


@fixture(name="fixture.headless.chrome")
def browser_headless_chrome(context):
    chrome_options = webdriver.ChromeOptions()
    chrome_options.headless = True
    try:
        web_driver_executable = check_web_driver_dir() + os.sep + CHROMEDRIVER + get_os_platform_to_execute_a_file_format()
        context.web = Web(webdriver.Chrome(executable_path=web_driver_executable,
                                           chrome_options=chrome_options,
                                           desired_capabilities=DESIRED_CAPS)
                          )
        yield from provide_webdriver(context, web, CHROMEDRIVER + " HEADLESS")
    except Exception as ex:
        context.web.close_and_quit()
        log_web_driver_error(ex)


@fixture(name="fixture.web.chrome")
def browser_chrome(context):
    chrome_options = webdriver.ChromeOptions()
    global web
    try:
        try:
            web_driver_executable = check_web_driver_dir() + os.sep \
                                    + CHROMEDRIVER + get_os_platform_to_execute_a_file_format()
            web = webdriver.Chrome(executable_path=web_driver_executable,
                                   desired_capabilities=get_chrome_options(chrome_options)
                                   )
        except:
            web = webdriver.Chrome()
        finally:
            yield from provide_webdriver(context, web, CHROMEDRIVER)
    except Exception as ex:
        context.web.close_and_quit()
        log_web_driver_error(ex)


def get_chrome_options(chrome_options):
    DESIRED_CAPS['browserName'] = "chrome"
    chrome_options.add_experimental_option("excludeSwitches", ["ignore-certificate-errors"])
    chrome_options.add_argument('disable-infobars')
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--start-maximized')


@fixture(name="fixture.web.edge")
def browser_edge(context):
    global web
    try:
        web_driver_executable = check_web_driver_dir() + os.sep + MSEDGEDRIVER + ".exe"
        web = webdriver.Edge(executable_path=web_driver_executable)
        yield from provide_webdriver(context, web, MSEDGEDRIVER)
    except Exception as ex:
        context.web.close_and_quit()
        log_web_driver_error(ex)


@fixture(name="fixture.web.firefox")
def browser_firefox(context):
    global web
    try:
        binary = FirefoxBinary("C:"+os.sep+"Program Files"+os.sep+"Mozilla Firefox"+os.sep+"firefox.exe")
        web_driver_executable = check_web_driver_dir() + os.sep + GECKODRIVER + get_os_platform_to_execute_a_file_format()
        web = webdriver.Firefox(firefox_binary=binary,
                                executable_path=web_driver_executable,
                                desired_capabilities=webdriver.DesiredCapabilities.FIREFOX
                                )
        yield from provide_webdriver(context, web, GECKODRIVER)
    except Exception as ex:
        context.web.close_and_quit()
        log_web_driver_error(ex)


@fixture(name="fixture.web.remote.chrome")
def browser_server_remote_chrome(context):
    try:
        web = webdriver.Remote(command_executor=Conf.get_web_remote_url() + "/wd/hub",
                               desired_capabilities={'browserName': 'chrome', 'javascriptEnabled': True})
        yield from provide_webdriver(context, web, "REMOTE CHROME")
    except Exception as ex:
        context.web.close_and_quit()
        log_web_driver_error(ex)


@fixture(name="fixture.web.remote.firefox")
def browser_server_remote_firefox(context):
    try:
        web = webdriver.Remote(command_executor=Conf.get_web_remote_url() + "/wd/hub",
                               desired_capabilities=webdriver.DesiredCapabilities.FIREFOX)
        yield from provide_webdriver(context, web, "REMOTE FIREFOX")
    except Exception as ex:
        context.web.close_and_quit()
        log_web_driver_error(ex)


@fixture(name="fixture.browserstack.chrome")
def browserstack_remote_chrome(context):
    try:
        web = webdriver.Remote(command_executor=BROWSERSTACK_URL,
                               desired_capabilities=webdriver.DesiredCapabilities.CHROME
                               )
        yield from provide_webdriver(context, web, "BROWSERSTACK CHROME")
    except Exception as ex:
        context.web.close_and_quit()
        log_web_driver_error(ex)


@fixture(name="fixture.browserstack.firefox")
def browserstack_remote_firefox(context):
    try:
        web = webdriver.Remote(command_executor=BROWSERSTACK_URL,
                               desired_capabilities=webdriver.DesiredCapabilities.FIREFOX
                               )
        yield from provide_webdriver(context, web, "BROWSERSTACK FIREFOX")
    except Exception as ex:
        context.web.close_and_quit()
        log_web_driver_error(ex)


@fixture(name="fixture.browserstack.edge")
def browserstack_remote_edge(context):
    try:
        web = webdriver.Remote(command_executor=BROWSERSTACK_URL,
                               desired_capabilities=webdriver.DesiredCapabilities.EDGE
                               )
        yield from provide_webdriver(context, web, "BROWSERSTACK EDGE")
    except Exception as ex:
        context.web.close_and_quit()
        log_web_driver_error(ex)


fixture_registry = {
    "fixture.headless.chrome": browser_headless_chrome,
    "fixture.web.chrome": browser_chrome,
    "fixture.web.edge": browser_edge,
    "fixture.web.firefox": browser_firefox,
    "fixture.web.remote.chrome": browser_server_remote_chrome,
    "fixture.web.remote.firefox": browser_server_remote_firefox,
    "fixture.browserstack.chrome": browserstack_remote_chrome,
    "fixture.browserstack.firefox": browserstack_remote_firefox,
    "fixture.browserstack.edge": browserstack_remote_edge
}
