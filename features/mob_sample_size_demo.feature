@mob_test
Feature: Entering the sample Size Calculator

	Scenario: Home tab calculation
		Given I click on Formulas tab
		And I click on Home tab
		When I focus and type "2" on Population field
		Then I see the value "425" calculated