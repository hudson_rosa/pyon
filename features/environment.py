from behave import use_fixture
from features import fixtures
from features.factory.validation.logging.base_logging import BaseLogging
from settings.environment_general_settings import Settings as Conf
from utils.TextColorUtil import TextColor as Color
from utils.StringsUtil import StringUtil


SKIP = 'skip'


def before_all(context):
    Conf.show_logo()


def before_feature(context, feature):
    print(Color.magenta(StringUtil.create_header_with_top_marker("", feature, "", "=")))
    if SKIP in feature.tags:
        feature.skip("Marked with @skip")
        return


def after_feature(context, feature):
    print("\n")


def before_scenario(context, scenario):
    print(Color.magenta(StringUtil.create_header_with_top_marker("", scenario, " :", "-")))
    print("\n")
    if SKIP in scenario.effective_tags:
        scenario.skip("Marked with @skip")
        return


def after_scenario(context, scenario):
    if "web" in scenario.effective_tags \
        or "mob" in scenario.effective_tags \
            and not SKIP in scenario.effective_tags \
            and scenario.status == "failed":
        context.web.take_screenshot("Failed")


def before_step(context, step):
    print(Color.cyan(StringUtil.create_header_statement("---- STEP ", step, " :")))


def after_step(context, step):
    if step.status == "failed":
        BaseLogging.log_short_status_failed()
    elif step.status == "passed":
        BaseLogging.log_short_status_passed()


def before_tag(context, tag):
    if tag == "fixture.headless.chrome":
        use_fixture(fixtures.browser_headless_chrome, context)
    if tag == "fixture.web.chrome":
        use_fixture(fixtures.browser_chrome, context)
    if tag == "fixture.web.edge":
        use_fixture(fixtures.browser_edge, context)
    if tag == "fixture.web.firefox":
        use_fixture(fixtures.browser_firefox, context)
    if tag == "fixture.web.remote.chrome":
        use_fixture(fixtures.browser_server_remote_chrome, context)
    if tag == "fixture.web.remote.firefox":
        use_fixture(fixtures.browser_server_remote_firefox, context)
    if tag == "fixture.browserstack.chrome":
        use_fixture(fixtures.browserstack_remote_chrome, context)
    if tag == "fixture.browserstack.firefox":
        use_fixture(fixtures.browserstack_remote_firefox, context)
    if tag == "fixture.browserstack.edge":
        use_fixture(fixtures.browserstack_remote_edge, context)
