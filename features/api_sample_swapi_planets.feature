@api_test
Feature: Checking different requests from Star Wars Planet API (SWAPI)

    Background: GET Planets SWAPI
        Given a request without an authentication

    @health_check
    Scenario: Validate the GET of a Planet using SWAPI
        Given a valid planet as "Kashyyyk" by ID in the SWAPI
        And the SWAPI URL supports planet research
        When the user sends a GET Http request using the URL with ID
        Then the response contains the planet expected

#    @health_check
#    Scenario: Validate the POST of a new Planet using SWAPI
#        Given a new planet called "Earth"
#        When the user sends a POST Http request using the URL
#        Then the response contains the planet expected
#
#    @health_check
#    Scenario: Validate the DELETE of a new Planet using SWAPI
#        Given a planet called "Earth"
#        And the user sends a POST Http request using the URL
#        When the user sends a DELETE Http request
#        Then the response shows that planet is deleted

    @contract
    Scenario: Check the Contract by a SWAPI Planet response body
        Given a request url "https://swapi.co/api/planets/3"
        And the response with 200 status with registers
        When the user sets a schema
        And sets the response body
        Then the json response matches the schema

    @functional
    Scenario Outline: Check different planets using SWAPI
        Given the valid parameters "<url>", "<planet>" and "<id>" to SWAPI
        When the user sends a GET Http request with these parameters
        Then the response for url contains the planet name

        Examples: Planets
        | url                           | planet   | id |
        | https://swapi.co/api/planets/ | Yavin IV | 3  |
        | https://swapi.co/api/planets/ | Hoth     | 4  |
        | https://swapi.co/api/planets/ | Naboo    | 8  |
        | https://swapi.co/api/planets/ | Kashyyyk | 14 |

    @functional
        @skip
    Scenario: Check the requests concurrency (X-Rate-Limit)
        Given a new request url "https://swapi.co/api/planets/8"
        When the user defines "100" requests to send in 1 min
        Then the SWAPI interrupt with 429 status
