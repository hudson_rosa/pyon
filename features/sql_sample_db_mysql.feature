@db_test
Feature: Connecting, populating and selecting "feature" registers in a MySQL database

    Background: Cleaning up the 'pyon_schema_tests' database
        Given the user can connect on database
        And reset the database with SQL statements

    Scenario: The user can create tables, populate and select registers
        Given the user create tables tb_test_resource, tb_test_type, tb_category and tb_feature
        When the user populates these tables
        Then the user can select with "join" between tb_feature, tb_test_type and tb_category tables
        And the result should show "4" registers of features
