from locust import HttpLocust, TaskSet, task


# Insert locustio lib in requirements.txt

class UserBehavior(TaskSet):

    @task
    def get_something(self):
        self.client.post("/reserve.php", {
            'fromPort': 'Paris', 'toPort': 'Buenos+Aires'
        })


class WebsiteUser(HttpLocust):
    task_set = UserBehavior
    host = "http://blazedemo.com"
    min_wait = 5000
    max_wait = 15000

