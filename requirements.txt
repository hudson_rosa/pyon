allure-behave
Appium-Python-Client
behave
behave-testrail-reporter
bs4
# cx_Oracle
django-rest-assured
jsonschema
# locustio
mysql-connector-python
mysql_connector
nodeenv
npm
# protobuf
python-string-utils
# pyzmq
requests
robotframework
robotframework-appiumlibrary
robotframework-seleniumlibrary
robotframework-selenium2library
selenium
urllib3