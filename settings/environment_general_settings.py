import json
import os
import time
# import cx_Oracle
import mysql.connector as mysql
from utils.OsUtil import OsUtil
from features.factory.validation.exception.RunningException import RunningException as RExc
from utils.TextColorUtil import TextColor as Color

CLEAN_REPORTS_ENABLED = 'clean_all_reports'
GENERATE_REPORTS_ENABLED = 'generate_allure_reports'
API_SHOW_RESPONSE_BODY = 'api_show_response_body'
API_REQUEST_TIMEOUT = 'api_request_timeout'
RUN_WEB_REMOTE = 'run_web_remote'
RUN_MOBILE_TEST = 'run_mobile_test'
WEB_BROWSER_TIMEOUT = 'web_browser_timeout'
WEB_DRIVER_DIR = 'web_driver_dir'
WEB_REMOTE_URL = 'web_remote_url'
WEB_REMOTE_STANDALONE = 'web_remote_selenium_standalone_path'

DEVICE_FARMS_NAME = 'device_farms_name'
MOBILE_PLATFORM_NAME = 'mobile_platform_name'
MOBILE_PLATFORM_VERSION = 'mobile_platform_version'
MOBILE_DEVICE_NAME = 'mobile_device_name'
MOBILE_BROWSER_NAME = 'mobile_browser_name'
MOBILE_APP_ID = 'mobile_app_id'
MOBILE_APP_PORT = 'mobile_app_port'
MOBILE_DEVICE_ORIENTATION = 'mobile_device_orientation'
MOBILE_AUTOMATION_NAME = 'mobile_automation_name'
MOBILE_UDID = 'mobile_udid'
MOB_DRIVER_DIR = 'mob_driver_dir'
MOB_APP_PATH = 'mob_app_path'
MOB_APP_ACTIVITY = 'mob_app_activity'

DEVICE_FARM_REMOTE_USER_KEY = 'device_farm_user_key'
DEVICE_FARM_ACCESS_KEY = 'device_farm_access_key'
DEVICE_FARM_ENDPOINT = 'device_farm_endpoint'

DB_TYPE = 'db_type'
DB_USER = 'db_user'
DB_PASSWORD = 'db_password'
DB_HOST = 'db_host'
DB_NAME = 'db_name'
DB_DRIVER_ABS_PATH = 'db_driver_abs_path'
UTF8_UPPER = 'UTF-8'
UTF8 = 'utf-8'

SEP = os.sep
JSON_LOAD: object = json.load(open(OsUtil.search_file_in_root_dir(".", "env_settings")))


class Settings:

    @staticmethod
    def get_resources_path_selenium_standalone():
        return str(f".resources{SEP}webdrivers_for_automation{SEP}selenium-server-standalone-3.141.59.jar")

    @staticmethod
    def get_resources_resources_webdriver():
        return str(f".resources{SEP}webdrivers_for_automation")

    @staticmethod
    def get_resources_path_mobile_apps():
        return str(f".resources{SEP}mobile_apps")

    @staticmethod
    def get_screenshot_path():
        return str(f".{SEP}_json_report{SEP}screenshots")

    @staticmethod
    def get_allure_reports():
        return str(JSON_LOAD[GENERATE_REPORTS_ENABLED]).lower().strip()

    @staticmethod
    def get_clean_all_reports():
        return str(JSON_LOAD[CLEAN_REPORTS_ENABLED]).lower().strip()

    @staticmethod
    def get_api_response_body():
        return str(JSON_LOAD[API_SHOW_RESPONSE_BODY]).lower().strip()

    @staticmethod
    def get_api_request_timeout():
        return str(JSON_LOAD[API_REQUEST_TIMEOUT])

    @staticmethod
    def get_run_web_remote():
        return str(JSON_LOAD[RUN_WEB_REMOTE]).lower().strip()

    @staticmethod
    def get_mobile_test_enabled():
        return str(JSON_LOAD[RUN_MOBILE_TEST]).lower().strip()

    @staticmethod
    def get_device_farms_name():
        return str(JSON_LOAD[DEVICE_FARMS_NAME]).lower().strip()

    @staticmethod
    def get_mobile_platform_name():
        return str(JSON_LOAD[MOBILE_PLATFORM_NAME])

    @staticmethod
    def get_mobile_app_id():
        return str(JSON_LOAD[MOBILE_APP_ID])

    @staticmethod
    def get_mobile_app_port():
        return str(JSON_LOAD[MOBILE_APP_PORT])

    @staticmethod
    def get_mobile_device_orientation():
        return str(JSON_LOAD[MOBILE_DEVICE_ORIENTATION])

    @staticmethod
    def get_mobile_platform_version():
        return str(JSON_LOAD[MOBILE_PLATFORM_VERSION])

    @staticmethod
    def get_mobile_device_name():
        return str(JSON_LOAD[MOBILE_DEVICE_NAME])

    @staticmethod
    def get_mobile_browser_name():
        return str(JSON_LOAD[MOBILE_BROWSER_NAME])

    @staticmethod
    def get_mobile_automation_name():
        return str(JSON_LOAD[MOBILE_AUTOMATION_NAME])

    @staticmethod
    def get_mob_driver_dir():
        return str(JSON_LOAD[MOB_DRIVER_DIR])

    @staticmethod
    def get_mob_app_path():
        return str(JSON_LOAD[MOB_APP_PATH]).replace("\\", SEP).strip()

    @staticmethod
    def get_mob_app_activity():
        return str(JSON_LOAD[MOB_APP_ACTIVITY]).strip()

    @staticmethod
    def get_mobile_udid():
        return str(JSON_LOAD[MOBILE_UDID])

    @staticmethod
    def get_web_remote_url():
        return str(JSON_LOAD[WEB_REMOTE_URL]).strip()

    @staticmethod
    def get_web_remote_selenium_standalone_path():
        return str(JSON_LOAD[WEB_REMOTE_STANDALONE]).replace("\\", SEP).strip()

    @staticmethod
    def get_browser_timeout():
        return int(JSON_LOAD[WEB_BROWSER_TIMEOUT])

    @staticmethod
    def get_web_driver_dir():
        return str(JSON_LOAD[WEB_DRIVER_DIR])

    @staticmethod
    def get_device_farm_endpoint():
        return str(JSON_LOAD[DEVICE_FARM_ENDPOINT])

    @staticmethod
    def get_device_farm_user_key():
        return str(JSON_LOAD[DEVICE_FARM_REMOTE_USER_KEY])

    @staticmethod
    def get_device_farm_access_key():
        return str(JSON_LOAD[DEVICE_FARM_ACCESS_KEY])

    # ------- DB CONNECTION -------
    @staticmethod
    def new_db_connection(json_db_config):
        if json_db_config is not None:
            return json.load(json_db_config)

    @staticmethod
    def execute_sql(any_sql, new_json_config):
        if new_json_config is not None:
            new_db_connection: object = json.loads(new_json_config)
            user = new_db_connection[DB_USER]
            password = new_db_connection[DB_PASSWORD]
            host = new_db_connection[DB_HOST]
            db = new_db_connection[DB_NAME]
            driver_path = os.path.abspath(new_db_connection[DB_DRIVER_ABS_PATH])
            db_type = new_db_connection[DB_TYPE]
        else:
            user = JSON_LOAD[DB_USER]
            password = JSON_LOAD[DB_PASSWORD]
            host = JSON_LOAD[DB_HOST]
            db = JSON_LOAD[DB_NAME]
            driver_path = os.path.abspath(JSON_LOAD[DB_DRIVER_ABS_PATH])
            db_type = JSON_LOAD[DB_TYPE]
        try:
            # if str(db_type).lower() == str('oracle').lower():
            #     Settings.connect_to_oracle(user, password, host, db, driver_path, str(any_sql).strip())
            if str(db_type).lower() == str('mysql').lower():
                Settings.connect_to_mysql(user, password, host, db, driver_path, str(any_sql).strip())
        except Exception as ex:
            RExc.with_raising_error("Impossible to connect with the database!", ex)

    # @staticmethod
    # def connect_to_oracle(user, password, host, db, driver_path, any_sql):
    #     os.chdir(driver_path)
    #     os.environ['NLS_LANG'] = '.utf8'
    #     conn = cx_Oracle.connect(f"{user}/{password}@{host}/{db}",
    #                              encoding=UTF8_UPPER,
    #                              nencoding=UTF8_UPPER)
    #     version = conn.version
    #     print(f"\nOracle version is: {version} -- Encoding {conn.encoding}; Nencoding {conn.nencoding}")
    #     sqlline = str(any_sql).encode(UTF8).strip()
    #     Settings.select_cursor(conn, sqlline)
    #     conn.commit()
    #     conn.close()

    @staticmethod
    def connect_to_mysql(user, password, host, db, driver_path, any_sql):
        if str(driver_path) != '':
            os.chdir(driver_path)
        os.environ['NLS_LANG'] = '.utf8'
        conn = None
        try:
            conn = mysql.connect(host=host,
                                 user=user,
                                 password=password,
                                 database=db)
        except:
            conn = mysql.connect(host=host,
                                 user=user,
                                 password=password)
        finally:
            version = conn.get_server_info()
            print(f"\nMySql version is: {version}")
            Settings.select_cursor(conn, any_sql)
            conn.commit()
            conn.close()

    @staticmethod
    def select_cursor(conn, any_sql):
        cursor = conn.cursor()
        try:
            if str(any_sql) is not None:
                print(Color.blue("Executing statement:"))
                print(Color.blue(any_sql))
                any_sql = str(any_sql).replace("\n", "").strip()
                cursor.execute(any_sql)
                Settings.check_results(cursor)
        except Exception as e:
            print(Color.red(f"\n------------- Error found:  {e}: ------------"))
        finally:
            cursor.close()
            time.sleep(1)

    @staticmethod
    def check_results(cursor):
        print(Color.green("\n------------- The query has been executed ------------"))
        while True:
            row = cursor.fetchone()
            if row is None:
                break
            print(Color.green(row))

    @staticmethod
    def show_logo():
        pyon_logo = Color.blue("""
                                              $                                                             
                            $$$$$$$$$,      ~$$                                                             
                         $$$$$      $$$    $$$                                                              
                       $$$,          $$   $$                 ,x.                                            
                     $$$             $$  $$               $$$$$$$$$                ,$~                      
                   $$$              ~$~ $$               $$       ~$$           :$$$$$$$                    
                  $$               $$$  $$        $$$$  $$     $$$  $$$       $$$,    ,$~   ,$$$$:          
                ~$$              $$$:   $$      $$$ ,$  $$       $$   $$    $$$        $$ $$$$::$$$$$~      
               $$$         ~$$$$$$      $$$ ~$$$$   $$   $$     :$:    $$:$$$          ,$$$          $$$:   
              $$$                        ~$$$$     $$     $$$$$$$$      $$$                            :$$~ 
             $$,                                  $$         $~                                           ~ 
            $$~                                  $$                                                         
           :$$                                  $$       ,$$$$    $$$$$   $      $  ,$$$  $        $ $$$$$  
           $$                                 ,$$       $     $  $     $ $      $  $   `$ $      $  $     $ 
          $$                                 $$$        $    $  $     $  $      $     .$$  $    $  $     $  
         $$                                 $$$         $$$$$   $..$$$  :$$$$$$$   $$$´¨$   $  $   $..$$$   
         $$                                $$          $     $  $$´     $      $  $     $    $$    $$´      
        $$                               ~$$           $    ,$  $   ,$  $      $ $,    $     $     $   ,$   
        $,                               +$           ,$$$$$´   `$$$´  $      $   `$$$´      |     `$$$´   
            
                     PYON BEHAVE - API & UI Test Automation | Created by: Hudson S. S. Rosa 
        
        """)
        return print(pyon_logo)
