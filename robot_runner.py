import os
import shutil
import time
from utils.TextColorUtil import TextColor as Color
from features.factory.validation.exception.RunningException import RunningException as Rexc
from settings.environment_general_settings import Settings as Conf


SEP = os.sep


def main():
    try:
        cleanup_reports()
        run_robot()
    except Exception as ex:
        Rexc.with_raising_error("Process is not allowed to execute.", ex)


def run_robot():
    try:
        print(Color.blue("\nRunning Robot tests..."))
        os.system(f'python -m robot --outputdir .{SEP}robot_features{SEP}_robot-results --timestampoutputs robot_features{SEP}*.robot')
    except RuntimeError as re:
        Rexc.with_raising_error("A problem occurred during the feature execution!", re)


def cleanup_reports():
    if Conf.get_clean_all_reports() == "true":
        path_robot_results = f".{SEP}robot_features{SEP}_robot-results"
        deletion_message = " directory deleted successfully!"
        try:
            if os.path.exists(path_robot_results):
                shutil.rmtree(path_robot_results)
                print(Color.green(path_robot_results + deletion_message))
            time.sleep(2)
        except FileNotFoundError as fnfe:
            Rexc.with_raising_error("Directory not found: ", fnfe)


class RobotRunner:
    if __name__ == "__main__":
        main()
