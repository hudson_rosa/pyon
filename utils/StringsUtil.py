import json
import math

class StringUtil:

    @staticmethod
    def is_not_blank_or_null(value):
        if value is not None or value != "":
            return value

    @staticmethod
    def get_marker_from_a_text_length(text, marker):
        string_length = str(text).__len__()
        count_str = 0
        new_string = ""
        for i in range(0, string_length):
            count_str = count_str + i
            new_string = new_string + str(marker)
        return new_string

    @staticmethod
    def create_header_statement(title, text, ends_with):
        return f"{str(title).upper()}{text}{ends_with}"

    @staticmethod
    def create_header_with_top_marker(title, text, ends_with, marker):
        return StringUtil.get_marker_from_a_text_length(str(title) + str(text), marker)\
               + "\n" + StringUtil.create_header_statement(title, text, ends_with)

    @staticmethod
    def indent_dict_body(dictionary):
        return json.dumps(dictionary, sort_keys=True, indent=4)

    @staticmethod
    def format_two_decimal_places(decimal_number):
        return "{:.2f}".format(decimal_number)

    @staticmethod
    def format_month_in_number_to_full_name(number_of_month):
        months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
        return months[int(number_of_month) - 1]

    @staticmethod
    def convert_currency_value_to_number(value, currency_symbol):
        if str(currency_symbol) in value:
            removed_currency = value.replace(str(currency_symbol), "")
            if ".00" in removed_currency:
                new_value = removed_currency.replace(".00", "")
                return int(new_value)
            else:
                return int(math.trunc(float(removed_currency)))