import uuid
import hashlib
import random
from datetime import date
from features.factory.validation.exception.RunningException import RunningException as Rexc


class DataEncrypted:

    @staticmethod
    def generate_random_data(length):
        length = int(length)
        random_number = random.randint(1, 9999999)
        string_to_hash = str(random_number) + str(date.today().strftime('%dd%mm%YY%HH%MM%SS'))
        subs_hash = DataEncrypted.parse_to_sha1(string_to_hash)
        final_hash = subs_hash[:length]
        print(f"Hash obtained: {final_hash}")
        return final_hash

    @staticmethod
    def parse_to_sha1(string_to_hash):
        try:
            hash_object = hashlib.sha1(bytes(string_to_hash, 'utf-8'))
            hex_dig = hash_object.hexdigest()
            return hex_dig
        except InterruptedError as ie:
            Rexc.with_raising_error("Parsing of object was not possible. ", ie)

    @staticmethod
    def hash_password(password):
        salt = uuid.uuid4().hex
        return hashlib.sha256(salt.encode() + password.encode()).hexdigest() + ':' + salt

    @staticmethod
    def check_password(hashed_password, user_password):
        password, salt = hashed_password.split(':')
        return password == hashlib.sha256(salt.encode() + user_password.encode()).hexdigest()
