import os
from utils.TextColorUtil import TextColor as Color
from features.factory.validation.exception.RunningException import RunningException as RExc

SEP = os.sep


def main():
    run_updates()


def run_updates():
    try:
        print(Color.blue("\nUpdating PIP..."))
        os.system(f"python -m pip install --upgrade pip")
        print(Color.blue("\nInstalling Python Environment..."))
        os.system(f"python -m pip install pipenv")
        print(Color.blue("\nSetting a new Python Environment..."))
        os.system("python -m venv venv")
        print(Color.blue("\nUpdating Behave..."))
        os.system(f"python -m pip install behave --user")
        print(Color.blue("\nUpdating all packages and resources..."))
        os.system(f"python -m pip install -r requirements.txt --user")
        print(Color.blue("\nInstalling Cython - No compile mode..."))
        os.system(f'pip install Cython --install-option="--no-cython-compile"')
        print(Color.blue("\nPackages installed:"))
        os.system(f"python -m pip list")
    except RuntimeError as re:
        RExc.with_raising_error("A problem occurred during the updating execution!", re)


class BehaveRunner:
    if __name__ == "__main__":
        main()
