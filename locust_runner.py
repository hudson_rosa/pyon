import os, sys
from utils.OsUtil import OsUtil
from utils.TextColorUtil import TextColor as Color
from features.factory.validation.exception.RunningException import RunningException as Rexc


SEP = os.sep


def main():
    try:
        run_locust()
    except Exception as ex:
        Rexc.with_raising_error("Process is not allowed to execute.", ex)


def run_locust():
    try:
        project_path = OsUtil.get_current_dir("locust_features")
        go_to_locust_package_path()
        print(Color.blue("\nRunning Locust for performance tests in http://****:8089"))
        os.system(f'locust -f {project_path}{SEP}locust_blaze_demo.py')
    except RuntimeError as re:
        Rexc.with_raising_error("A problem occurred during the feature execution!", re)


def go_to_locust_package_path():
    package_folder = "site-packages"
    python_folder = "Python"
    python_paths = sys.path
    for python_dirs in python_paths:
        if python_dirs.__contains__(package_folder) \
                and python_dirs.__contains__(python_folder):
            print(python_dirs)
            locust_path = python_dirs + SEP + "locustio-0.8.1.dist-info"
            os.system(f'cd {locust_path}')
            break


class LocustRunner:
    if __name__ == "__main__":
        main()
