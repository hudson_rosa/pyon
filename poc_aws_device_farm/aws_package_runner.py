import os
from utils.TextColorUtil import TextColor as Color
from features.factory.validation.exception.RunningException import RunningException as RExc
from utils.OsUtil import OsUtil


SEP = os.sep
ROOT = OsUtil.get_current_dir("mobile")
ZIP_NAME = "test_bundle"


def main():
    try:
        run_locust()
    except Exception as ex:
        RExc.with_raising_error("Process is not allowed to execute.", ex)


def run_locust():
    try:
        print(Color.blue("\nRunning package for AWS Device Farms tests..."))
        os.system(f'cd {ROOT}')
        os.system(f'python -m pip freeze > {ROOT}{SEP}requirements.txt')
        os.system(f'python -m pip wheel --wheel-dir {ROOT}{SEP}wheelhouse -r {ROOT}{SEP}requirements.txt')
        if OsUtil.has_os_platform_name("linux") or OsUtil.has_os_platform_name("darwin"):
            os.system(f'zip -r {ZIP_NAME}.zip {ROOT}{SEP}tests{SEP} {ROOT}{SEP}wheelhouse{SEP} {ROOT}{SEP}requirements.txt')
        else:
            OsUtil.zip_folder(ROOT, ZIP_NAME, "zip")
    except RuntimeError as re:
        RExc.with_raising_error("A problem occurred during the feature execution!", re)


class LocustRunner:
    if __name__ == "__main__":
        main()
