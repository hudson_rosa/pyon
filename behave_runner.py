import multiprocessing
import os
import shutil
import json
import time
from behave.__main__ import main as behave_main
from settings.environment_general_settings import Settings as Conf
from features.factory.web.sel_standalone_setup import GridFactory
from features.factory.mobile.mobile_factory import MobFactory
from utils.TextColorUtil import TextColor as Color
from features.factory.validation.exception.RunningException import RunningException as Rexc
from utils.OsUtil import OsUtil


SEP = os.sep

"""
    ATTENTION: Insert additional command parameters in 'behave.ini'  file
"""


def main():
    cleanup_reports()
    thread_selstd = None
    try:
        if Conf.get_run_web_remote() == "true":
            thread_selstd = GridFactory.start_new_thread_for_selenium_server()
            thread_selstd.start()
        if Conf.get_mobile_test_enabled() == "true":
            app_path = os.path.abspath(Conf.get_mob_app_path())
            run_mobile_with_browserstack(app_path)
            run_mobile_locally()
        run_behave_allure()
        generate_report()
        GridFactory.kill_thread(thread_selstd)
    except multiprocessing.ProcessError as pe:
        Rexc.with_raising_error("Process is not allowed to execute.\n "
                                "Please, check the configurations in 'env_settings' and 'behave.ini' flags", pe)
    finally:
        GridFactory.kill_thread(thread_selstd)
        GridFactory.force_kill_java_process()
        MobFactory.force_kill_adb_process()


def cleanup_reports():
    if Conf.get_clean_all_reports() == "true":
        path_allure_results = f".{SEP}features{SEP}_allure-results"
        path_json_report = f".{SEP}features{SEP}_json_report"
        deletion_message = " directory deleted successfully!"
        try:
            if os.path.exists(path_allure_results):
                shutil.rmtree(path_allure_results)
                print(Color.green(path_allure_results + deletion_message))
            if os.path.exists(path_json_report):
                shutil.rmtree(path_json_report)
                print(Color.green(path_json_report + deletion_message))
            time.sleep(2)
        except FileNotFoundError as fnfe:
            Rexc.with_raising_error("Directory not found: ", fnfe)


def run_behave_allure():
    try:
        print(Color.blue("\nRunning Behave tests..."))
        behave_main(f'-f allure_behave.formatter:AllureFormatter -o features/_json_report features')
    except RuntimeError as re:
        Rexc.with_raising_error("A problem occurred during the feature execution!", re)


def run_mobile_with_browserstack(app_path):
    if Conf.get_device_farms_name() == "browserstack":
        user = Conf.get_device_farm_user_key()
        key = Conf.get_device_farm_access_key()
        api_url = "https://api-cloud.browserstack.com/app-automate/upload"
        result = os.popen(f'curl -u "{user}:{key}" -X POST "{api_url}" -F "file=@{app_path}"').read()
        app_id = json.loads(result)
        OsUtil.set_env_var('MOB_APP_ID', str(app_id['app_url']))


def run_mobile_locally():
    if Conf.get_device_farms_name() == "local":
        app_id = Conf.get_mobile_app_id()
        port = Conf.get_mobile_app_port()
        device_ip_port = str(f'{app_id}:{port}')
        try:
            os.system(f'adb disconnect')
            os.system(f'adb tcpip {port}')
        except Exception as ex:
            Rexc.with_raising_error("You need to install the Android Debug Bridge (ADB) and Appium to execute Android tests", ex)
        finally:
            result = os.popen(f'adb connect {device_ip_port}').read()
            if str(result).lower().__contains__("connected"):
                OsUtil.set_env_var('MOB_APP_ID', str(f'{device_ip_port}'))


def generate_report():
    allure_path = f".{SEP}.resources{SEP}allure-2.10.0{SEP}bin{SEP}allure"
    allure_results = f".{SEP}features{SEP}_allure-results"
    report_json_feed = f".{SEP}features{SEP}_json_report"
    if Conf.get_allure_reports() == "true":
        print(Color.blue("\n\nGenerating Allure Report..."))
        if OsUtil.has_os_platform_name("linux"):
            os.system(f"chmod 777 {allure_path}")
        os.system(f"{allure_path} generate -o {allure_results} {report_json_feed}")
        os.system(f'{allure_path} open {allure_results}')
        print(Color.yellow(f"See the reports results in: " + OsUtil.search_file_in_root_dir(f"{SEP}features{SEP}_allure-results", "index.html")))


class BehaveRunner:
    if __name__ == "__main__":
        main()
