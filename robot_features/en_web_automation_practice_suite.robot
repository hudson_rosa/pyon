*** Settings ***
Documentation         Formal ATDD for Automation Practice website.
...                   Using a single Gherkin style
Library               Selenium2Library
Library               Screenshot
Test Teardown         Run Keyword If Test Failed    Capture Page Screenshot
Suite Teardown        Close Browser

*** Variables ***
# Test Data
${browser}            Chrome
${url}                http://automationpractice.com/index.php
${product_name}       Blouse

# Locators
${elm_text_search}    xpath=//*[@id="search_query_top"]
${elm_button_search}  xpath=//*[@id="searchbox"]/button
${elm_label_product}  xpath=//*[@id="center_column"]/ul/li/div/div[2]/h5/a

*** Test Cases ***
Scenario: TC-0001 User can search a specific product
    [Documentation]   As a user I can open the Automation Practice website and search a product
    Given browser is opened and maximized to Automation Practice website
    When user types "Blouse" in the search field
    And clicks in the search button
    Then the product is found in the results

*** Keywords ***
Given browser is opened and maximized to Automation Practice website
    Open Browser  ${url}  ${browser}
    Maximize Browser Window
When user types "${nome_produto}" in the search field
    Input text  ${elm_text_search}  ${product_name}
And clicks in the search button
    Click Button  ${elm_button_search}
    Press Key  ${elm_button_search}  \\34
Then the product is found in the results
    Wait Until Element Contains  ${elm_label_product}  ${product_name}
