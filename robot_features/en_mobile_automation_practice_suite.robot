*** Settings ***
Documentation               Formal ATDD for Automation Practice website.
...                         Using a single Gherkin style
Library                     AppiumLibrary
Library                     Screenshot
Test Teardown               Run Keyword If Test Failed    Capture Page Screenshot
Suite Teardown              Close Browser

*** Variables ***
${DEV.APPIUM_SERVER}        http://127.0.0.1:4723/wd/hub
${DEV.PLATFORM_VERSION}     6.0.1
${DEV.PLATFORM_NAME}        Android
${DEV.DEVICE_NAME}          MOB_TEST
${DEV.PACKAGE_NAME}         x
${DEV.ACTIVITY_NAME}        x

# Test Data
${browser}                  Chrome
${url}                      http://automationpractice.com/index.php
${product_name}             Blouse

# Locators
${elm_text_search}          xpath=//*[@id="search_query_top"]
${elm_button_search}        xpath=//*[@id="searchbox"]/button
${elm_label_product}        xpath=//*[@id="center_column"]/ul/li/div/div[2]/h5/a

*** Test Cases ***
Scenario: TC-0001 User can search a specific product
    [Documentation]   As a user I can open the Automation Practice website and search a product
    Given new session app is opened in Automation Practice website
    When user types "Blouse" in the search field
    And clicks in the search button
    Then the product is found in the results

*** Keywords ***
Given new session app is opened in Automation Practice website
    Open Application   ${DEV.APPIUM_SERVER}  platformName=${DEV.PLATFORM_NAME}  platformVersion=${DEV.PLATFORM_VERSION}
    ...                deviceName=${DEV.DEVICE_NAME}  appPackage=${DEV.PACKAGE_NAME}  appActivity=${DEV.ACTIVITY_NAME}
    Open Browser  ${url}  ${browser}
When user types "${nome_produto}" in the search field
    Input text  ${elm_text_search}  ${product_name}
And clicks in the search button
    Click Button  ${elm_button_search}
Then the product is found in the results
    Wait Until Element Contains  ${elm_label_product}  ${product_name}
